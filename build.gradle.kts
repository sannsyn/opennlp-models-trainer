import org.jetbrains.intellij.platform.gradle.IntelliJPlatformType
import org.jetbrains.intellij.platform.gradle.extensions.intellijPlatform
import org.jetbrains.kotlin.gradle.dsl.JvmTarget

val onlpVersion = "2.5.0"
val jacksonVersion = "2.18.1"
val jerseyVersion = "3.1.9"

repositories {
    mavenCentral()
    intellijPlatform {
        defaultRepositories()
    }
}

plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "2.0.21"
    id("org.jetbrains.intellij.platform") version "2.1.0" // https://plugins.jetbrains.com/docs/intellij/tools-intellij-platform-gradle-plugin.html#usage
    id("org.jetbrains.grammarkit") version "2022.3.2.2" // https://plugins.jetbrains.com/docs/intellij/tools-gradle-grammar-kit-plugin.html#usage
}

group = "com.sannsyn"
version = "0.9.6"

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html#ide-configuration
intellijPlatform {
    pluginConfiguration {
        // Configure the plugin ID and version
        id = "com.sannsyn.opennlpmodelstrainer"
        version = "0.9.6"
        name = "OpenNLP Models Trainer"
        description = "A plugin for OpenNLP. Edit training files and generate OpenNLP models directly from your favourite IDE."
    }
    pluginVerification {
        ides {
            ide(IntelliJPlatformType.IntellijIdeaCommunity, "2024.3") // IntelliJ IDEA Community
        }
    }
}

grammarKit {
    jflexRelease.set("1.9.1") // https://github.com/jflex-de/jflex/releases
    grammarKitRelease.set("2022.3.2.2") // https://plugins.jetbrains.com/docs/intellij/tools-gradle-grammar-kit-plugin.html
    intellijRelease.set("243.21565.193") // https://www.jetbrains.com/intellij-repository/releases / com.jetbrains.intellij.platform
}

sourceSets {
    main {
        java {
            srcDir { listOf("src/main/kotlin", "src/main/gen") }
        }
        kotlin {
            srcDir { "src/main/gen" }
        }
    }
}

kotlin {
    compilerOptions {
        apiVersion.set(org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_1_9)
        jvmTarget = JvmTarget.JVM_21
    }
}

tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "21"
        targetCompatibility = "21"
    }

    patchPluginXml {
        sinceBuild.set("243.21565.193")
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }

//    publishPlugin {
//        token.set(System.getenv("NLP_TRAINER_PERSONAL_PUBLISH_TOKEN"))
//    }

}

dependencies {

    intellijPlatform {
        intellijIdeaCommunity("2024.3")
        bundledPlugins("com.intellij.java") // needed for the projectStructureDetector
        instrumentationTools()
        pluginVerifier()
    }

    implementation("org.apache.opennlp:opennlp-tools:$onlpVersion") // https://central.sonatype.com/artifact/org.apache.opennlp/opennlp-tools/versions
    implementation("org.apache.opennlp:opennlp-uima:$onlpVersion") // https://central.sonatype.com/artifact/org.apache.opennlp/opennlp-uima/versions

    //implementation("org.apache.opennlp:opennlp-brat-annotator:$onlpVersion") // https://central.sonatype.com/artifact/org.apache.opennlp/opennlp-brat-annotator/versions
    // opennlp-brat-annotator uses the following four dependencies with vulnerabilities.
    // So until we leave brat v2.1.0, explicitly import these versions:
//    implementation("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion") // https://central.sonatype.com/artifact/com.fasterxml.jackson.core/jackson-databind/versions
//    implementation("com.fasterxml.jackson.core:jackson-annotations:$jacksonVersion") // https://central.sonatype.com/artifact/com.fasterxml.jackson.core/jackson-annotations/versions
//    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion") // https://central.sonatype.com/artifact/com.fasterxml.jackson.module/jackson-module-kotlin/versions
//    implementation("org.glassfish.jersey.core:jersey-common:$jerseyVersion") // https://central.sonatype.com/artifact/org.glassfish.jersey.core/jersey-common/versions

}
