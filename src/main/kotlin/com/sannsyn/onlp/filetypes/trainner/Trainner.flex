package com.sannsyn.onlp.filetypes.trainner;

import com.intellij.psi.tree.IElementType;
import com.sannsyn.onlp.psi.TrainnerTypes;
import com.intellij.psi.TokenType;

%%

%class TrainnerLexer
%implements com.intellij.lexer.FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

TAG_START      = "<START:"
TAG_END        = "<END>"
SPLIT          = "<SPLIT>"
NEWLINE        = \R // Cf. http://jflex.de/manual.html#LexRules. Two newlines in a row --> two tokens
SPACEANDTAB    = [\t\ \ ] // second space on this line is &nbsp;
TAG_TYPE_CHARS = [a-z]+
TAG_CLOSER     = >
LT             = < // in order to have tags lacking space (the case for <SPLIT> tags!) recognized
TEXT_CHAR      = [^\r\n\t\ \ <]

%state IN_VALUE, IN_START_TAG, IN_TAG_TYPE

%%

<YYINITIAL> {TAG_START}             { yybegin(IN_START_TAG); return TrainnerTypes.TAG_START; }

<IN_START_TAG> {TAG_TYPE_CHARS}     { yybegin(IN_TAG_TYPE); return TrainnerTypes.TAG_TYPE; }

<IN_TAG_TYPE> {TAG_CLOSER}          { yybegin(IN_VALUE); return TokenType.WHITE_SPACE; }

<IN_VALUE> ({SPLIT}|{SPACEANDTAB})+ { return TokenType.WHITE_SPACE; } // note: SPLIT returns WHITE_SPACE when IN_VALUE ...

<IN_VALUE> {TAG_END}                { yybegin(YYINITIAL); return TrainnerTypes.TAG_END; }

{TAG_END}                           { yybegin(YYINITIAL); return TokenType.ERROR_ELEMENT; }

<YYINITIAL> {LT}|{TEXT_CHAR}+       { return TrainnerTypes.WORD; }

<IN_VALUE> {LT}|{TEXT_CHAR}+        { return TrainnerTypes.ENTITY_WORD; }

<YYINITIAL> {SPACEANDTAB}+          { return TokenType.WHITE_SPACE; }

<IN_VALUE> {SPACEANDTAB}+           { return TokenType.WHITE_SPACE; }

{NEWLINE}                           { yybegin(YYINITIAL); return TrainnerTypes.NEWLINE; } // returns to YYINITIAL, so tags cannot span over lines

//{LT}                                { }

.                                   { return TokenType.BAD_CHARACTER; }

