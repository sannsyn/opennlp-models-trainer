package com.sannsyn.onlp.filetypes.trainner

import com.intellij.lang.*
import com.intellij.lexer.Lexer
import com.intellij.openapi.project.Project
import com.intellij.psi.*
import com.intellij.psi.tree.*
import com.sannsyn.onlp.parser.TrainnerParser
import com.sannsyn.onlp.psi.TrainnerFile
import com.sannsyn.onlp.psi.TrainnerTypes

class TrainnerParserDefinition : ParserDefinition {

  override fun createLexer(project: Project): Lexer {
    return TrainnerLexerAdapter()
  }

  override fun getWhitespaceTokens(): TokenSet {
    return WHITE_SPACES
  }

  override fun getCommentTokens(): TokenSet {
    return TokenSet.EMPTY
  }

  override fun getStringLiteralElements(): TokenSet {
    return TokenSet.EMPTY
  }

  override fun createParser(project: Project): PsiParser {
    //System.out.println("TrainnerParserDefinition will return a TrainnerParser instance ...");
    return TrainnerParser()
  }

  override fun getFileNodeType(): IFileElementType {
    return FILE
  }

  override fun createFile(viewProvider: FileViewProvider): PsiFile {
    return TrainnerFile(viewProvider)
  }

//  Deprecated, probably a question of spelling, but this function is not
//  demanded by the interface, it is given as a default method, so we may leave it out.
//  override fun spaceExistanceTypeBetweenTokens(left: ASTNode, right: ASTNode): ParserDefinition.SpaceRequirements {
//    return ParserDefinition.SpaceRequirements.MAY
//  }

  override fun createElement(node: ASTNode): PsiElement {
    return TrainnerTypes.Factory.createElement(node)
  }

}

val WHITE_SPACES: TokenSet = TokenSet.WHITE_SPACE
val FILE = IFileElementType(TrainnerLanguage)