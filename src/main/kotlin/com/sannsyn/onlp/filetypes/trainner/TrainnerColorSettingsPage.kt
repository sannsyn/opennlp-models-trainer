package com.sannsyn.onlp.filetypes.trainner

import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighter
import com.intellij.openapi.options.colors.*
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

import javax.swing.*

class TrainnerColorSettingsPage : ColorSettingsPage {

  override fun getIcon(): Icon {
    return TrainxIcons.TRAINNER
  }

  override fun getHighlighter(): SyntaxHighlighter {
    return TrainnerSyntaxHighlighter()
  }

  override fun getDemoText(): String {
    return """This is an example.
        Markup looks like <START:whatever> this<SPLIT>example <END> .
        """
  }

  override fun getAdditionalHighlightingTagToDescriptorMap(): Map<String, TextAttributesKey>? {
    return null
  }

  override fun getAttributeDescriptors(): Array<AttributesDescriptor> {
    return DESCRIPTORS
  }

  override fun getColorDescriptors(): Array<ColorDescriptor> {
    return ColorDescriptor.EMPTY_ARRAY
  }

  override fun getDisplayName(): String {
    return "Trainner"
  }

}

private val DESCRIPTORS = arrayOf(
  AttributesDescriptor("Entity", TrainnerSyntaxHighlighter.ENTITY),
  AttributesDescriptor("Tag", TrainnerSyntaxHighlighter.TAG)
)