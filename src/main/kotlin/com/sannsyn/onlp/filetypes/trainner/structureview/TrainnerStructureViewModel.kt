package com.sannsyn.onlp.filetypes.trainner.structureview

import com.intellij.ide.structureView.StructureViewModel
import com.intellij.ide.structureView.StructureViewModelBase
import com.intellij.ide.structureView.StructureViewTreeElement
import com.intellij.ide.util.treeView.smartTree.Sorter
import com.intellij.psi.PsiFile
import com.sannsyn.onlp.psi.TrainnerFile
import com.sannsyn.onlp.psi.impl.TrainnerTaggedEntityImpl

class TrainnerStructureViewModel(psiFile: PsiFile) : StructureViewModelBase(psiFile, TrainnerStructureViewElement(psiFile)),
                                                   StructureViewModel.ElementInfoProvider {

    override fun getSorters(): Array<Sorter> {
        return arrayOf(Sorter.ALPHA_SORTER)
        // When sortet alphabetically, they are sorted on getStringPresentation ignoring case
    }


    override fun isAlwaysShowsPlus(element: StructureViewTreeElement): Boolean {
        return element.value is TrainnerFile
    }

    override fun isAlwaysLeaf(element: StructureViewTreeElement): Boolean {
        return element.value is TrainnerTaggedEntityImpl
    }

}
