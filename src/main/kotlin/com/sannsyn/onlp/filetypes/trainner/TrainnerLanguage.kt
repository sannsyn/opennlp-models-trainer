package com.sannsyn.onlp.filetypes.trainner

import com.intellij.lang.Language

object TrainnerLanguage : Language("Trainner") {
    fun readResolve(): Any = TrainnerLanguage
    override fun getDisplayName() = "NER training file"
}

