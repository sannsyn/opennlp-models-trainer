package com.sannsyn.onlp.filetypes.trainner

import com.intellij.openapi.fileTypes.LanguageFileType
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

class TrainnerFileType: LanguageFileType(TrainnerLanguage) {

  override fun getName() = "Trainner"

  override fun getDescription() = TrainxBundle.message("Filetypes.trainner.description")

  override fun getDefaultExtension() = "trainner"

  override fun getIcon() = TrainxIcons.TRAINNER

}
