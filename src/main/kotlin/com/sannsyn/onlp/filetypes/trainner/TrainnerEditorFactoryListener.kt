package com.sannsyn.onlp.filetypes.trainner

import com.intellij.openapi.editor.event.EditorFactoryEvent
import com.intellij.openapi.editor.event.EditorFactoryListener
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager

class TrainnerEditorFactoryListener : EditorFactoryListener {

    override fun editorCreated(event: EditorFactoryEvent) {

        event.editor.project?.let {
            event.editor.settings.isUseSoftWraps = TrainxProjectSettingsManager.getInstance(it)?.state?.softWrapTrainFiles ?: true
            return
        }

        event.editor.settings.isUseSoftWraps = true
    }

    override fun editorReleased(event: EditorFactoryEvent) {}

}