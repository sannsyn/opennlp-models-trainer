package com.sannsyn.onlp.filetypes.trainner.editor

import com.intellij.lang.ASTNode
import com.intellij.lang.folding.FoldingBuilder
import com.intellij.lang.folding.FoldingDescriptor
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.FoldingGroup
import com.intellij.openapi.util.TextRange
import com.intellij.psi.util.PsiTreeUtil
import com.sannsyn.onlp.psi.TrainnerTaggedEntity

import java.util.ArrayList

class TrainnerFoldingBuilder : FoldingBuilder {

    override fun buildFoldRegions(node: ASTNode, document: Document): Array<FoldingDescriptor> {
        // Why are all of them checked in one go?
        //val group = FoldingGroup.newGroup("named-entity-tags")
        val descriptors = ArrayList<FoldingDescriptor>()
        val taggedEntities = PsiTreeUtil.findChildrenOfType(node.psi, TrainnerTaggedEntity::class.java)

        taggedEntities.map {  taggedEntity ->
            taggedEntity.namedEntityAsReadableText?.let {
                val range = TextRange(taggedEntity.textRange.startOffset + 1,
                        taggedEntity.textRange.endOffset - 1) // + and - one in order to keep the tag markers

                descriptors.add(
                        // a new folding group for each tagged entity, so they may expand and collapse individually
                        object : FoldingDescriptor(taggedEntity.node, range, /* group */ FoldingGroup.newGroup(it) ) {
                    override fun getPlaceholderText(): String {
                        return it
                    }
                })
            }
        }

        return descriptors.toTypedArray()
    }

    override fun getPlaceholderText(node: ASTNode): String {
        return "<NER>" // default value
    }

    override fun isCollapsedByDefault(node: ASTNode): Boolean {
        return true
    }
}

// http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/folding_builder.html?search=SimpleProperty