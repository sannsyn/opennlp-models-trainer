package com.sannsyn.onlp.filetypes.trainner

import com.intellij.lexer.Lexer
import com.intellij.openapi.editor.*
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase
import com.intellij.psi.TokenType
import com.intellij.psi.tree.IElementType
import com.sannsyn.onlp.psi.TrainnerTypes

import com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey

class TrainnerSyntaxHighlighter : SyntaxHighlighterBase() {

  override fun getHighlightingLexer(): Lexer {
    return TrainnerLexerAdapter()
  }

  override fun getTokenHighlights(tokenType: IElementType): Array<TextAttributesKey> {
    return when (tokenType) {
      TrainnerTypes.SPLIT        -> SEPARATOR_KEYS

      TrainnerTypes.NAMED_ENTITY -> ENTITY_KEYS

      TrainnerTypes.TAG_TYPE     -> TAG_TYPE_KEYS

      TrainnerTypes.TAG_END,
      TrainnerTypes.TAG_START    -> TAG_KEYS

      TokenType.BAD_CHARACTER  -> BAD_CHAR_KEYS

//      TrainnerTypes.WORD         -> EMPTY_KEYS

      else                     -> EMPTY_KEYS
    }
  }

  companion object {

    val SEPARATOR = createTextAttributesKey("TRAINNER_SEPARATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN)
    val ENTITY = createTextAttributesKey("TRAINNER_ENTITY", DefaultLanguageHighlighterColors.KEYWORD)
    val TAG_TYPE = createTextAttributesKey("TRAINNER_TAG_TYPE", DefaultLanguageHighlighterColors.IDENTIFIER)
    val TAG = createTextAttributesKey("TRAINNER_TAG", DefaultLanguageHighlighterColors.STRING)
    val COMMENT = createTextAttributesKey("TRAINNER_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT)
    val BAD_CHARACTER = createTextAttributesKey("TRAINNER_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER)

    private val BAD_CHAR_KEYS = arrayOf(BAD_CHARACTER)
    private val SEPARATOR_KEYS = arrayOf(SEPARATOR)
    private val TAG_KEYS = arrayOf(TAG)
    private val TAG_TYPE_KEYS = arrayOf(TAG_TYPE)
    private val COMMENT_KEYS = arrayOf(COMMENT)
    private val ENTITY_KEYS = arrayOf(ENTITY)
    private val EMPTY_KEYS = arrayOf<TextAttributesKey>()
  }

}
