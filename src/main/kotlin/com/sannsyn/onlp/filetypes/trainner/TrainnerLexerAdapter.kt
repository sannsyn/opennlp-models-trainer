package com.sannsyn.onlp.filetypes.trainner

import com.intellij.lexer.FlexAdapter

import java.io.Reader

class TrainnerLexerAdapter : FlexAdapter(TrainnerLexer(null as Reader?))
