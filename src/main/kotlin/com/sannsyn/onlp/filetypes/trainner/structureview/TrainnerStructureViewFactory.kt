package com.sannsyn.onlp.filetypes.trainner.structureview

import com.intellij.ide.structureView.*
import com.intellij.lang.PsiStructureViewFactory
import com.intellij.openapi.editor.Editor
import com.intellij.psi.PsiFile
import com.sannsyn.onlp.psi.TrainnerTaggedEntity

class TrainnerStructureViewFactory : PsiStructureViewFactory {

    override fun getStructureViewBuilder(psiFile: PsiFile): StructureViewBuilder {
        return object : TreeBasedStructureViewBuilder() {
            override fun createStructureViewModel(editor: Editor?): TextEditorBasedStructureViewModel {
                return TrainnerStructureViewModel(psiFile).withSuitableClasses(TrainnerTaggedEntity::class.java)
            }
        }
    }

}
