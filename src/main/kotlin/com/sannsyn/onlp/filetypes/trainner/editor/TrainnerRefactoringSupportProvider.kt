package com.sannsyn.onlp.filetypes.trainner.editor

import com.intellij.lang.refactoring.RefactoringSupportProvider
import com.intellij.psi.PsiElement
import com.sannsyn.onlp.psi.TrainnerTaggedEntity

class TrainnerRefactoringSupportProvider : RefactoringSupportProvider() {

    override fun isMemberInplaceRenameAvailable(element: PsiElement, context: PsiElement?): Boolean {
        return element is TrainnerTaggedEntity
    }

}
