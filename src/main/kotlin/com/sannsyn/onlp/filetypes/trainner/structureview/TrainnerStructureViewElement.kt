package com.sannsyn.onlp.filetypes.trainner.structureview

import com.intellij.ide.projectView.PresentationData
import com.intellij.ide.structureView.StructureViewTreeElement
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement
import com.intellij.ide.util.treeView.smartTree.TreeElement
import com.intellij.navigation.ItemPresentation
import com.intellij.psi.NavigatablePsiElement
import com.intellij.psi.util.PsiTreeUtil
import com.sannsyn.onlp.psi.TrainnerFile
import com.sannsyn.onlp.psi.TrainnerParagraph
import com.sannsyn.onlp.psi.TrainnerTaggedEntity
import java.util.*

class TrainnerStructureViewElement(private val element: NavigatablePsiElement) : StructureViewTreeElement, SortableTreeElement {

    override fun getValue(): Any {
        return element
    }

    override fun navigate(requestFocus: Boolean) {
        element.navigate(requestFocus)
    }

    override fun canNavigate(): Boolean {
        return element.canNavigate()
    }

    override fun canNavigateToSource(): Boolean {
        return element.canNavigateToSource()
    }

    override fun getAlphaSortKey(): String {
        val name = element.name
        return name ?: ""
    }

    override fun getPresentation(): ItemPresentation {
        val presentation = element.presentation
        return presentation ?: PresentationData()
    }

    override fun getChildren(): Array<TreeElement> {

        var treeElements = arrayOf<TreeElement>()

        if (element is TrainnerFile) {
            val paragraphs =
                    PsiTreeUtil.getChildrenOfType(element, TrainnerParagraph::class.java) ?: return treeElements

            paragraphs.map { paragraph ->
                treeElements = treeElements.plus(
                        PsiTreeUtil.getChildrenOfType(paragraph, TrainnerTaggedEntity::class.java)
                                ?.map { TrainnerStructureViewElement(it as TrainnerTaggedEntity) }
                                ?: Collections.emptyList()
                )
            }
        }

        return treeElements
    }

}