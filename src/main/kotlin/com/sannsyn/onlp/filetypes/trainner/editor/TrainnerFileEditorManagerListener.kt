package com.sannsyn.onlp.filetypes.trainner.editor

import com.intellij.openapi.editor.EditorFactory
import com.intellij.openapi.editor.event.EditorMouseEvent
import com.intellij.openapi.editor.event.EditorMouseListener
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileEditor.FileEditorManagerListener
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.psi.PsiDocumentManager
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBLabel
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.actions.OneClickEntityMarkupAction
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.filetypes.traintok.TraintokFileType
import com.sannsyn.onlp.project.askForAndSetDisplayedEntityName
import com.sannsyn.onlp.project.fileToJblabel
import com.sannsyn.onlp.project.guessCurrentEntity
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager
import java.awt.BorderLayout
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import javax.swing.BorderFactory
import javax.swing.JLabel


class TrainnerFileEditorManagerListener(val myProject: Project) : FileEditorManagerListener {

    override fun fileClosed(manager: FileEditorManager, file: VirtualFile) {
        //fileToLabel.remove(file)
        fileToJblabel.remove(file)
        //println("This file is being closed: $file, so removing from the fileToLabel map, which now has ${TrainxProjectStartupActivity.fileToJblabel.size} elements.")
    }

    override fun fileOpened(manager: FileEditorManager, file: VirtualFile) {

        when (file.fileType) {
            is TrainnerFileType -> trainnerFileOpened(manager, file)
            is TrainsentFileType, is TraintokFileType -> { /* everything OK */ }
            else -> { /* println("What on earth is this? A " + file.fileType.description + "?!") */ }
        }

    }

    fun traintokFileOpened(manager: FileEditorManager, file: VirtualFile) {
        // no-op
    }

    fun trainsentFileOpened(manager: FileEditorManager, file: VirtualFile) {
        // no-op
    }

    private fun trainnerFileOpened(manager: FileEditorManager, file: VirtualFile) {
        //println("Yay! It's a trainner file!")

        val fileEditor = manager.getSelectedEditor(file) ?: return
        val document = FileDocumentManager.getInstance().getDocument(file) ?: return


        val markupCheckbox = JBCheckBox(TrainxBundle.message("settingsPanel.mouseClickMarkupMode"))
        markupCheckbox.isSelected = TrainxProjectSettingsManager.getInstance(myProject)?.state?.mouseClickMarkupMode ?: true
        markupCheckbox.addActionListener {
            TrainxProjectSettingsManager.getInstance(myProject)?.state?.mouseClickMarkupMode = markupCheckbox.isSelected
            fileToJblabel.forEach { label ->
                label.value.components.forEach {
                    if (it is JBCheckBox) {
                        it.isSelected = markupCheckbox.isSelected
                    }
                }
                // funny, this doesn't seem to trigger a new action on each of them
            }
        }

        val topLabel = JBLabel()
        topLabel.border = BorderFactory.createEmptyBorder(3, 3, 3, 3)
        //label.text = statusMessage(document, myProject) // could be cool, but ...
        topLabel.text = " " //"" Current markup entity: " // TODO: replace with some setting somewhere
        topLabel.layout = BorderLayout(20, 20)
        topLabel.add(markupCheckbox, BorderLayout.EAST)

        fileToJblabel[file] = topLabel

        topLabel.add(JLabel(" Current entity name: "), BorderLayout.WEST) // TODO: extract string to resource file

        val entityLabel =
                JBLabel(guessCurrentEntity(PsiDocumentManager.getInstance(myProject).getPsiFile(document), myProject, false)
                        ?: "")
        entityLabel.toolTipText = "Double click to change"
        topLabel.add(entityLabel, BorderLayout.CENTER)

        entityLabel.addMouseListener(object : MouseListener {
            override fun mouseReleased(e: MouseEvent?) {}
            override fun mouseEntered(e: MouseEvent?) {}
            override fun mouseExited(e: MouseEvent?) {}
            override fun mousePressed(e: MouseEvent?) {}

            override fun mouseClicked(event: MouseEvent?) {
                event?.consume()
                if (event?.button != 3 && event?.clickCount != 2) {
                    return
                }
                askForAndSetDisplayedEntityName(myProject, file, entityLabel.text)
            }
        })

//                document.addDocumentListener(object : DocumentListener {
//                    override fun documentChanged(event: DocumentEvent) {
//                        val psiFile = PsiDocumentManager.getInstance(myProject).getPsiFile(document) ?: return
//                        println("Hey, something has happened! Source: " + event.source)
//                    }
//                })


        manager.addTopComponent(fileEditor, topLabel)
        //println("We just added the top component ...")

        val editors = EditorFactory.getInstance().getEditors(document, myProject)
        editors.forEach {
            // there really should be only one ... ?
            it.addEditorMouseListener(object : EditorMouseListener {
                override fun mouseReleased(event: EditorMouseEvent) {
                    if (TrainxProjectSettingsManager.getInstance(myProject)?.state?.mouseClickMarkupMode == false) {
                        return
                    }

                    if (event.mouseEvent.clickCount == 0) {
                        val psiFile = PsiDocumentManager.getInstance(myProject).getPsiFile(document) ?: return
                        OneClickEntityMarkupAction.toggleMarkup(psiFile, myProject, it)
                    }
                }

                override fun mouseEntered(e: EditorMouseEvent) {}
                override fun mouseClicked(e: EditorMouseEvent) {}
                override fun mouseExited(e: EditorMouseEvent) {}
                override fun mousePressed(e: EditorMouseEvent) {}

            }) // end addEditorMouseListener
        } // end editors.foreach

    } // end fileOpened
}

