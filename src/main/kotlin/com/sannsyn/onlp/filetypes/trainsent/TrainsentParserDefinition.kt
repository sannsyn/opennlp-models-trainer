package com.sannsyn.onlp.filetypes.trainsent

import com.intellij.lang.ASTFactory
import com.intellij.lang.ASTNode
import com.intellij.openapi.fileTypes.PlainTextParserDefinition
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PlainTextTokenTypes
import com.intellij.psi.tree.IFileElementType
import com.sannsyn.onlp.psi.TrainsentFile

class TrainsentParserDefinition : PlainTextParserDefinition() {

  // Simply copying from our parent class (since createParser(Project project) throws ...)
  private val trainsentFileElementType = object : IFileElementType(TrainsentLanguage) {
    override fun parseContents(chameleon: ASTNode) = ASTFactory.leaf(PlainTextTokenTypes.PLAIN_TEXT, chameleon.chars)
  }

  override fun getFileNodeType() = trainsentFileElementType

  override fun createFile(viewProvider: FileViewProvider) = TrainsentFile(viewProvider)

}
