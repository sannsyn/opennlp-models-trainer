package com.sannsyn.onlp.filetypes.trainsent

import com.intellij.openapi.fileTypes.LanguageFileType
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

class TrainsentFileType : LanguageFileType(TrainsentLanguage) {

    override fun getName() = "Trainsent"

    override fun getDescription() = TrainxBundle.message("Filetypes.trainsent.description")

    override fun getDefaultExtension() = "trainsent"

    override fun getIcon() = TrainxIcons.TRAINSENT

}
