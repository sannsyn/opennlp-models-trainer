package com.sannsyn.onlp.filetypes.trainsent

import com.intellij.lang.Language

object TrainsentLanguage : Language("Trainsent") {
    fun readResolve(): Any = TrainsentLanguage
    override fun getDisplayName() = "Sentence detector training file"
}
