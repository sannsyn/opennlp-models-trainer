package com.sannsyn.onlp.filetypes.trainx.editor

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.Annotator
import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.editor.HighlighterColors
import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.openapi.editor.colors.TextAttributesKey
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiElement
import com.intellij.util.ui.JBUI
import java.awt.Color
import kotlin.math.max


class TrainxStripesAnnotator : Annotator {

    private var cachedLineCount = 0
    private var alreadyHandled = false

    /**
     * Annotates the specified PSI element. It is guaranteed to be executed in non-reentrant fashion,
     * i.e. there will be no call of this method for this instance before previous call get completed.
     * Multiple instances of the annotator might exist simultaneously, though.
     *
     * @param element to annotate.
     * @param holder  the container which receives annotations created by the plugin.
     */
    override fun annotate(element: PsiElement, holder: AnnotationHolder) {

        val document = PsiDocumentManager.getInstance(element.project).getDocument(element.containingFile) ?: return

        if (alreadyHandled && document.lineCount == cachedLineCount) {
            // https://intellij-support.jetbrains.com/hc/en-us/community/posts/360003376980-Striped-document?page=1#community_comment_360000489759
            // TODO: surveil this for a while ...
            return
        }

        cachedLineCount = document.lineCount
        alreadyHandled = true
        val darkEditor = EditorColorsManager.getInstance().isDarkEditor
        val stripe: Color = if (darkEditor) {
            JBUI.CurrentTheme.EditorTabs.background().brighter() // than blackish
        } else {
            JBUI.CurrentTheme.EditorTabs.background().darker() // than white
        }
        val alternateColour = TextAttributesKey.createTextAttributesKey(if (darkEditor) "DARK_GREY_LINE" else "LIGHT_GREY_LINE", HighlighterColors.TEXT).apply {
            defaultAttributes.backgroundColor = stripe
        }
        val background = TextAttributesKey.createTextAttributesKey("BACKGROUND_LINE", HighlighterColors.NO_HIGHLIGHTING).apply {
            defaultAttributes.backgroundColor = JBUI.CurrentTheme.EditorTabs.background()
        }

        for (i in 0 until document.lineCount) {

            val endOffset = max(document.getLineEndOffset(i) + 1, element.textRange.endOffset)
            val line = TextRange(document.getLineStartOffset(i), endOffset)
            if (!element.textRange.contains(line)) {
                continue
            }

            val markup = if (i % 2 != 0) alternateColour else background
            holder
                .newAnnotation(HighlightSeverity.INFORMATION, "")
                .range(line)
                //.textAttributes(markup)
                .enforcedTextAttributes(markup.defaultAttributes)
                .create()
        } // end for loop

        // ... so the holder has document.lineCount elements. Let's hope it will not slow us down.

    }

}
