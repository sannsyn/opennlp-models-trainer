package com.sannsyn.onlp.filetypes.trainx

import com.intellij.openapi.util.IconLoader

object TrainxIcons {

  @JvmField internal val TRAINX    = IconLoader.getIcon("icons/trainx.png", javaClass)

  @JvmField internal val TRAINNER  = IconLoader.getIcon("icons/trainner.png", javaClass)

  @JvmField internal val TRAINSENT = IconLoader.getIcon("icons/trainsent.png", javaClass)

  @JvmField internal val TRAINTOK = IconLoader.getIcon("icons/traintok.png", javaClass)

}
