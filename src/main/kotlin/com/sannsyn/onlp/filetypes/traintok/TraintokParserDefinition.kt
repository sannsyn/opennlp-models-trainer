package com.sannsyn.onlp.filetypes.traintok

import com.intellij.lang.ASTFactory
import com.intellij.lang.ASTNode
import com.intellij.openapi.fileTypes.PlainTextParserDefinition
import com.intellij.psi.FileViewProvider
import com.intellij.psi.PlainTextTokenTypes
import com.intellij.psi.PsiFile
import com.intellij.psi.tree.IFileElementType
import com.sannsyn.onlp.psi.TraintokFile

class TraintokParserDefinition : PlainTextParserDefinition() {

    // TODO: THIS FILE TYPE IS SUPPOSED TO ACCEPT THE <SPLIT> TAGS, AND THEY SHOULD BE MARKED UP, BUT MAYBE
    //       WE DON'T NEED A FULL LANGUAGE DEFINITION FOR IT, WE MIGHT JUST SETTLE WITH AN ANNOTATOR?

    // Simply copying from our parent class (since createParser(Project project) throws ...)
    private val traintokFileElementType = object : IFileElementType(TraintokLanguage) {
        override fun parseContents(chameleon: ASTNode): ASTNode {
            val chars = chameleon.chars
            return ASTFactory.leaf(PlainTextTokenTypes.PLAIN_TEXT, chars)
        }
    }

    override fun getFileNodeType(): IFileElementType {
        return traintokFileElementType
    }

    override fun createFile(viewProvider: FileViewProvider): PsiFile {
        return TraintokFile(viewProvider)
    }

}
