package com.sannsyn.onlp.filetypes.traintok

import com.intellij.openapi.fileTypes.LanguageFileType
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

import javax.swing.*

class TraintokFileType: LanguageFileType(TraintokLanguage) {

  override fun getName() = "Traintok"

  override fun getDescription() = TrainxBundle.message("Filetypes.traintok.description")

    override fun getDefaultExtension() = "traintok"

  override fun getIcon(): Icon = TrainxIcons.TRAINTOK

}
