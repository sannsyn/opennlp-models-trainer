package com.sannsyn.onlp.filetypes.traintok

import com.intellij.lang.Language

object TraintokLanguage : Language("Traintok") {
    fun readResolve(): Any = TraintokLanguage
    override fun getDisplayName() = "Tokenization training file"
}

