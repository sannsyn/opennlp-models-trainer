package com.sannsyn.onlp.ui

import com.intellij.openapi.wm.ToolWindowEP

class TrainxToolWindow : ToolWindowEP() {

    companion object {
        const val TRAINX_TOOLWINDOW_ID = "Trainx"
    }

    init {
        id = TRAINX_TOOLWINDOW_ID
        anchor="bottom"
        factoryClass="com.sannsyn.onlp.ui.TrainxToolWindowFactory"
        icon="/com/sannsyn/onlp/icons/trainner.png"
    }
}