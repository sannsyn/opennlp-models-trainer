package com.sannsyn.onlp.ui

import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.DefaultActionGroup
import com.intellij.openapi.ui.SimpleToolWindowPanel
import com.intellij.ui.components.JBScrollPane
import com.sannsyn.onlp.actions.AboutAction
import java.awt.EventQueue
import java.awt.GridLayout
import javax.swing.JPanel
import javax.swing.JTextArea

// Tool window panel to capture output from OpenNLP tools and such
class TrainxPanel : SimpleToolWindowPanel(false, false) {

    private val output: JTextArea = JTextArea()

    init {
        //layout = BorderLayout()
        add(JBScrollPane(output))

        val toolbarGroup = DefaultActionGroup()
        toolbarGroup.add(AboutAction())
        toolbarGroup.addSeparator()

        val toolBarPanel = JPanel(GridLayout())
        val actionToolbar = ActionManager.getInstance().createActionToolbar("ToolWindowToolbar", toolbarGroup, false)
        toolBarPanel.add(actionToolbar.component)
        toolbar = toolBarPanel

    }

    fun appendText(text: String) {
        if (EventQueue.isDispatchThread()) {
            output.append(text)
            //output.caretPosition = output.text.length
        } else {
            EventQueue.invokeLater { appendText(text) }
        }
    }
}
