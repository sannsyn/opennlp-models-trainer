package com.sannsyn.onlp.ui

import com.intellij.execution.filters.TextConsoleBuilderFactory
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory


class TrainxToolWindowFactory : ToolWindowFactory, DumbAware {

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val consoleView = TextConsoleBuilderFactory.getInstance().createBuilder(project).console
        //val content = toolWindow.contentManager.factory.createContent(consoleView.component, "Tool output", true)
        //content.displayName = "SET DISPLAY NAME"
        //toolWindow.contentManager.addContent(content)
    }

}


//class ToolWindowGUI {
//    val textArea = JTextArea()
//
//    init {
//        val frame = JFrame("ObjectivesGUI")
//        frame.contentPane = textArea
//        textArea.text = ""
//        textArea.isEditable = false
//        frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
//        frame.pack()
//        frame.isVisible = true
//    }
//}