package com.sannsyn.onlp.settings

import com.intellij.openapi.components.*
import com.intellij.openapi.project.Project

/**
 * This file holds the project's settings.
 * These are settings that pertain to this particular
 * project, and they may differ from the general
 * settings found in a neighbouring file.
 *
 * In addition, this file is the projectConfigurable used to display
 * settings among the settings panels.
 */

@Service(Service.Level.PROJECT)
@State(name = "TrainxSettings", storages = [Storage("trainx.xml")])
class TrainxProjectSettingsManager : SimplePersistentStateComponent<TrainxProjectSettings>(TrainxProjectSettings()) {

    private var trainxProjectSettings = TrainxProjectSettings()

    override fun loadState(state: TrainxProjectSettings) {
        trainxProjectSettings = state
    }

    companion object {
        fun getInstance(project: Project): TrainxProjectSettingsManager? = project.getService(TrainxProjectSettingsManager::class.java)
    }

}

enum class TrainxProjectTokenModel {
    WHITESPACE,
    SIMPLE,
    LEARNABLE
}

/** Values seen here are the default values */
data class TrainxProjectSettings(
    var language: String = "",
    var mouseClickMarkupMode: Boolean = true,

    var softWrapTrainFiles: Boolean = true,

    var trainxProjectTokenModel: TrainxProjectTokenModel = TrainxProjectTokenModel.WHITESPACE,

    var taggedEntityTypes: MutableSet<String> = mutableSetOf(),

    var noOfTrainingIterations: Int = 100,
    var eosCharacters: String = "!?."
) : BaseState()

