package com.sannsyn.onlp.settings

import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.project.Project
import org.jetbrains.annotations.Nls

import javax.swing.*

class TrainxProjectConfigurable(val project: Project) : Configurable {

    private val settingsPanel: TrainxProjectSettingsPanel = TrainxProjectSettingsPanel(false, project)

//    init {
//        println("We're constructing the TrainxProjectConfigurable for ${project.name}!")
//    }

    /**
     * Returns the visible name of the configurable component.
     * Note, that this method must return the display name
     * that is equal to the display name declared in XML
     * to avoid unexpected errors.
     *
     * @return the visible name of the configurable component
     */
    @Nls
    override fun getDisplayName(): String {
        return "OpenNLP Models Trainer"
    }

    /**
     * Creates new Swing form that enables user to configure the settings.
     * Usually this method is called on the EDT, so it must not take a long time.
     *
     * This place is designed to allocate resources (subscriptions/listeners etc.)
     *
     * @return new Swing form to show, or `null` if it cannot be created
     * @see .disposeUIResources
     */
    override fun createComponent(): JComponent {
        settingsPanel.settingsToGUI(TrainxProjectSettingsManager.getInstance(project)?.state ?: TrainxProjectSettings())
        return settingsPanel
    }

    /**
     * Indicates whether the Swing form was modified or not.
     * This method is called very often, so it should not take a long time.
     *
     * @return `true` if the settings were modified, `false` otherwise
     */
    override fun isModified() = settingsPanel.isModified(TrainxProjectSettingsManager.getInstance(project)?.state ?: TrainxProjectSettings())

    /**
     * Stores the settings from the Swing form to the configurable component.
     * This method is called on EDT upon user's request.
     *
     * @throws ConfigurationException if values cannot be applied
     */
    override fun apply() {
        if (settingsPanel.validateFields()) {
            settingsPanel.saveGuiDataToSettings(TrainxProjectSettingsManager.getInstance(project)?.state ?: TrainxProjectSettings())
        }
    }
}
