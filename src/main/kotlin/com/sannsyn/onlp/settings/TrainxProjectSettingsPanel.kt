package com.sannsyn.onlp.settings

import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.project.Project
import com.intellij.ui.components.*
import com.intellij.ui.components.panels.HorizontalLayout
import com.intellij.ui.components.panels.VerticalLayout
import com.intellij.util.ui.UIUtil
import com.sannsyn.onlp.TrainxBundle
import javax.swing.*
import javax.swing.text.NumberFormatter

import java.text.NumberFormat
import java.util.*

class TrainxProjectSettingsPanel(withHeading: Boolean, project: Project?) : JBPanel<JBPanel<*>>(VerticalLayout(5), true) {
    private val languageTextfield = JBTextField()
    private val formatter = NumberFormatter(NumberFormat.getInstance()).apply {
        //formatter.valueClass = kotlin.Int::class.java
        minimum = 0
        maximum = Integer.MAX_VALUE
        allowsInvalid = false
        // If you want the value to be committed on each keystroke instead of focus lost
        commitsOnValidEdit = true
    }
    private val numberOfIterationsTextField = JFormattedTextField(formatter).apply {
        columns = 5
    }

    private val clickMarkupModeCheckBox = JBCheckBox(TrainxBundle.message("settingsPanel.mouseClickMarkupMode"))
    private val eosCharsTextfield = JBTextField()

    private val whitespaceTrainxProjectTokenModelButton = makeJBRadioButton(TrainxProjectTokenModel.WHITESPACE.name)
    private val simpleTrainxProjectTokenModelButton = makeJBRadioButton(TrainxProjectTokenModel.SIMPLE.name)
    private val learnableTrainxProjectTokenModelButton = makeJBRadioButton(TrainxProjectTokenModel.LEARNABLE.name)
    private val buttonGroup = ButtonGroup().apply {
        add(whitespaceTrainxProjectTokenModelButton)
        add(simpleTrainxProjectTokenModelButton)
        add(learnableTrainxProjectTokenModelButton)
    }
    private val tokenPanel = JBPanel<JBPanel<*>>(VerticalLayout(2)).apply {
        add(whitespaceTrainxProjectTokenModelButton)
        add(simpleTrainxProjectTokenModelButton)
        add(learnableTrainxProjectTokenModelButton)
    }


    init {

        if (withHeading) {
            add(JBLabel("OpenNLP Models Trainer Project Settings", UIUtil.ComponentStyle.LARGE))
        }

        val onlpPanel = JBPanel<JBPanel<*>>(VerticalLayout(5))
            //.withBorder(MetalBorders.InternalFrameBorder())
        add(onlpPanel)

        val lPanel = JBPanel<JBPanel<*>>(HorizontalLayout(2))
        lPanel.add(JBLabel("Project Language: "))
        lPanel.add(languageTextfield)
        onlpPanel.add(lPanel)

        onlpPanel.add(clickMarkupModeCheckBox)

        val numPanel = JBPanel<JBPanel<*>>(HorizontalLayout(2))
        @Suppress("DialogTitleCapitalization")
        numPanel.add(JBLabel(TrainxBundle.message("settingsPanel.NoOfIterationsWhenTrainingModel")))
        numPanel.add(numberOfIterationsTextField)
        onlpPanel.add(numPanel)

        val eoscPanel = JBPanel<JBPanel<*>>(HorizontalLayout(2))
        eoscPanel.add(JBLabel(TrainxBundle.message("settingsPanel.EosCharacters")))
        eoscPanel.add(eosCharsTextfield)
        onlpPanel.add(eoscPanel)

        // TODO: add some explanations in a tooltip (or several) here
        val tokenMarginPanel = JBPanel<JBPanel<*>>(HorizontalLayout(2))
        tokenMarginPanel.add(JBLabel(TrainxBundle.message("settingsPanel.tokenModelUsedInProject")))
        tokenMarginPanel.add(tokenPanel)
        onlpPanel.add(tokenMarginPanel)

        if (project != null) {
            settingsToGUI(TrainxProjectSettingsManager.getInstance(project)?.state ?: TrainxProjectSettings())
        }
    }

    fun settingsToGUI(trainxProjectSettings: TrainxProjectSettings) {

        // onlp settings:
        languageTextfield.text = trainxProjectSettings.language
        clickMarkupModeCheckBox.isSelected = trainxProjectSettings.mouseClickMarkupMode
        numberOfIterationsTextField.value = trainxProjectSettings.noOfTrainingIterations
        eosCharsTextfield.text = trainxProjectSettings.eosCharacters
        (tokenPanel.getComponent(trainxProjectSettings.trainxProjectTokenModel.ordinal) as JBRadioButton).isSelected = true
    }

    fun saveGuiDataToSettings(trainxProjectSettings: TrainxProjectSettings) {

        // onlp settings:
        trainxProjectSettings.language = languageTextfield.text
        // in the formatted number field, remove no-break-space before turning into an int:
        trainxProjectSettings.noOfTrainingIterations = numberOfIterationsTextField.text.replace(" ", "").toIntOrNull() ?: trainxProjectSettings.noOfTrainingIterations
        trainxProjectSettings.mouseClickMarkupMode = clickMarkupModeCheckBox.isSelected
        trainxProjectSettings.eosCharacters = String(eosCharsTextfield.text.toSet().toCharArray()) // pass by a set to remove duplicates
        trainxProjectSettings.trainxProjectTokenModel = TrainxProjectTokenModel.valueOf(buttonGroup.selection.actionCommand)
    }

    fun isModified(trainxProjectSettings: TrainxProjectSettings): Boolean {

        // onlp settings:
        if (languageTextfield.text != trainxProjectSettings.language ||
            eosCharsTextfield.text.toSet() != trainxProjectSettings.eosCharacters.toSet() ||
            clickMarkupModeCheckBox.isSelected != trainxProjectSettings.mouseClickMarkupMode ||
            trainxProjectSettings.trainxProjectTokenModel != TrainxProjectTokenModel.valueOf(buttonGroup.selection.actionCommand)
        )
            return true
        return numberOfIterationsTextField.text.toIntOrNull() != trainxProjectSettings.noOfTrainingIterations
    }

    @Throws(ConfigurationException::class)
    fun validateFields(): Boolean {
        if (languageTextfield.text.isNullOrBlank()) {
            throw ConfigurationException(TrainxBundle.message("settingsPanel.errors.languageFieldCannotBeEmpty"))
        }
        if (eosCharsTextfield.text.isNullOrBlank()) {
            // Will this make the tool unusable for languages that have no sentence separators? (Are there any such (written) languages?)
            throw ConfigurationException(TrainxBundle.message("settingsPanel.errors.eosCharactersFieldCannotBeEmpty"))
        }
        return true
    }

    private fun makeJBRadioButton(value: String) =
        JBRadioButton(value.lowercase(Locale.getDefault())).apply { actionCommand  = value}

}
