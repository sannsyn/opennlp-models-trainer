package com.sannsyn.onlp.project

import com.intellij.openapi.vfs.newvfs.BulkFileListener
import com.intellij.openapi.vfs.newvfs.events.*
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.module.TrainxModuleBuilder

object TrainxProjectFilesMonitor : BulkFileListener {
    override fun after(events: List<VFileEvent>) {
        events.filter {
            it.path.contains(TrainxModuleBuilder.TRAININGFILES_FOLDER_NAME) &&
                    it.file?.fileType is TrainnerFileType &&
                    (it is VFileCopyEvent || it is VFileCreateEvent || it is VFileMoveEvent || it is VFilePropertyChangeEvent)
        }

        .forEach {
            // create / delete / update / copy
            println("event = " + it.javaClass.name)
            println("File type: " + it.file?.fileType)
            if (it is VFilePropertyChangeEvent) {
                println("change: ${it.propertyName} changed from ${it.oldValue} into ${it.newValue}.")
            }

            // Surveillance spot: new files detected here
        }
    }
}
