package com.sannsyn.onlp.project

import com.intellij.ide.plugins.PluginManager
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.fileEditor.FileEditorManagerListener
import com.intellij.openapi.module.ModuleUtil
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity
import com.intellij.openapi.ui.InputValidatorEx
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.util.Key
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.psi.PsiFile
import com.intellij.ui.components.JBLabel
import com.sannsyn.onlp.filetypes.trainner.editor.TrainnerFileEditorManagerListener
import com.sannsyn.onlp.filetypes.trainner.structureview.TrainnerStructureViewElement
import com.sannsyn.onlp.module.TrainxModuleType
import com.sannsyn.onlp.psi.TrainnerTaggedEntity
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager
import java.io.File
import java.util.regex.Pattern

class TrainxProjectStartupActivity : ProjectActivity {

    override suspend fun execute(project: Project) {

        if (!ModuleUtil.hasModulesOfType(project, TrainxModuleType())) {
            // nothing here for us!
            return
        }

        val connection = project.messageBus.connect()

        connection.subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER, TrainnerFileEditorManagerListener(project))
        connection.subscribe(VirtualFileManager.VFS_CHANGES, TrainxProjectFilesMonitor)

    } // end runActivity


//    override fun projectClosed() {
//        println("PROJECT CLOSED!")
//    }

}

fun askForAndSetDisplayedEntityName(project: Project, file: VirtualFile, current: String) {
    val entityName = Messages.showInputDialog(project,
            "New entity for mouse click markup: ", // TODO: replace with resource string
            "Change current mouse click markup entity", // TODO: replace with resource string
            Messages.getQuestionIcon(),
            current,
            object : InputValidatorEx {
                override fun getErrorText(inputString: String?): String? {
                    return if (checkInput(inputString)) null else "Only lowercase letters are allowed in the entity name"
                }

                override fun checkInput(inputString: String?): Boolean {
                    return inputString != null && Pattern.compile("^[a-z]+$").matcher(inputString).matches()
                }

                override fun canClose(inputString: String?) = checkInput(inputString)
            }
    ) ?: return

    setDisplayedEntityName(project, file, entityName)
}

fun setDisplayedEntityName(project: Project, file: VirtualFile, entityName: String) {
    TrainxProjectSettingsManager.getInstance(project)?.state?.taggedEntityTypes?.add(entityName)
    fileToJblabel[file]?.components?.forEach {
        if (it is JBLabel) {
            // the other one is a JLabel, so only the entity field is in a JBLabel
            it.text = entityName
        }
    }
    file.putUserData(trainnerFileCurrentEntityKey, entityName)
}

fun guessCurrentEntity(psiFile: PsiFile?, project: Project?, ask: Boolean): String? {
    // First alternative: this is already set:
    var toReturn = psiFile?.virtualFile?.getUserData(trainnerFileCurrentEntityKey)
    if (toReturn != null) {
        //println("FOUND IT ON FIRST TRY: $toReturn")
        return toReturn
    }

    // Second alternative: most used entity name in file
    if (psiFile != null) {
        val children = TrainnerStructureViewElement(psiFile).children
        //println("Number of markup occurrences in file: " + children.size)
        toReturn =
                children
                        .filter { it is TrainnerStructureViewElement && it.value is TrainnerTaggedEntity }
                        .groupBy { ((it as TrainnerStructureViewElement).value as TrainnerTaggedEntity).tagType }
                        .toList()
                        .maxBy { it.second.size }.first

        if (toReturn != null) {
            //println("FOUND IT AS MOST USED ENTITY NAME: $toReturn")
            return toReturn
        }
    }

    // If we still have no value for the named entity setting, ask the user:
    return if (ask)
        Messages.showInputDialog(project,
                "Please define this named entity: ", // TODO: replace with resource string
                "No Current Entity is Registered", // TODO: replace with resource string
                Messages.getQuestionIcon())
    else null

}

val trainnerFileCurrentEntityKey = Key.create<String>("com.sannsyn.onlp.trainnerFileCurrentEntityKey")

// TODO: consider if this map should reside somewhere within the trainner folder, since it is specific for that file type
val fileToJblabel = mutableMapOf<VirtualFile, JBLabel>()
// TODOTOO: we shouldn't have to use the filename for this dependency. We should find it without specifying the version.
fun openNlpToolsJar() = getLibraryJarPath("opennlp-tools-2.5.0.jar")
private fun getLibraryJarPath(jarFileName: String): File? {
    val runtimePluginId = PluginManager.getPluginByClass(TrainxProjectStartupActivity::class.java)?.pluginId
    val runtimePlugin = PluginManagerCore.getPlugin(runtimePluginId) ?: return null

    val opennlpToolsJar = runtimePlugin.pluginPath.resolve("lib/$jarFileName").toFile()
    return if (opennlpToolsJar.exists()) opennlpToolsJar.absoluteFile else null
}
