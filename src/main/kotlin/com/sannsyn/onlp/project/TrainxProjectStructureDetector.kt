package com.sannsyn.onlp.project

import com.intellij.ide.util.importProject.ModuleDescriptor
import com.intellij.ide.util.importProject.ProjectDescriptor
import com.intellij.ide.util.projectWizard.importSources.DetectedProjectRoot
import com.intellij.ide.util.projectWizard.importSources.DetectedSourceRoot
import com.intellij.ide.util.projectWizard.importSources.ProjectFromSourcesBuilder
import com.intellij.ide.util.projectWizard.importSources.ProjectStructureDetector
import com.intellij.openapi.module.JavaModuleType
import com.intellij.openapi.module.Module
import com.intellij.openapi.roots.ModifiableRootModel
import com.intellij.openapi.util.io.FileUtilRt
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.module.TrainxDetectedSourceRoot
import com.sannsyn.onlp.module.TrainxModuleBuilder

import java.io.File
import java.util.*

class TrainxProjectStructureDetector : ProjectStructureDetector() {

    private val trainnerFileType = TrainnerFileType()
    override fun detectRoots(dir: File,
                             children: Array<File>,
                             base: File,
                             result: MutableList<DetectedProjectRoot>): DirectoryProcessingResult {
        for (child in children) {
            if (FileUtilRt.extensionEquals(child.name, trainnerFileType.defaultExtension)) {
                var root: File?
                for (pattern in arrayOf("resources/trainingfiles", "resources")) {
                    root = findParentLike(pattern, dir, base)
                    if (root != null) {
                        result.add(TrainxDetectedSourceRoot(root))
                        println("Here's a file we like: $root")
                        return DirectoryProcessingResult.SKIP_CHILDREN
                    }
                }
            }
        }
        return DirectoryProcessingResult.PROCESS_CHILDREN
    }

    private fun findParentLike(pattern: String, dir: File?, limit: File): File? {
        @Suppress("NAME_SHADOWING") var dir = dir
        val names = pattern.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        while (dir != null && dir.path.startsWith(limit.path)) {
            if (names[0] == dir.name && checkParents(dir, names)) {
                return dir
            }

            dir = dir.parentFile
        }
        return null
    }

    private fun checkParents(dir: File, names: Array<String>): Boolean {
        @Suppress("NAME_SHADOWING") var dir = dir
        for (name in names) {
            if (dir.name == name) {
                dir = dir.parentFile
            } else {
                return false
            }
        }
        return true
    }

    override fun setupProjectStructure(roots: Collection<DetectedProjectRoot>,
                                       projectDescriptor: ProjectDescriptor,
                                       builder: ProjectFromSourcesBuilder) {
        println("setupProjectStructure receives roots: $roots, projectDescriptor: ${projectDescriptor}, builder: ${builder.baseProjectPath}")
        if (roots.isNotEmpty() && !builder.hasRootsFromOtherDetectors(this)) {
            println("roots wasn't empty.")
            var modules: MutableList<ModuleDescriptor> = projectDescriptor.modules
            if (modules.isEmpty()) {
                println("projectDescriptor.modules was empty")
                modules = ArrayList()
                for (root in roots) {
                    if (root is DetectedSourceRoot) {
                        modules.add(object : ModuleDescriptor(File(builder.baseProjectPath), JavaModuleType.getModuleType(), root) {

                            override fun updateModuleConfiguration(module: Module, rootModel: ModifiableRootModel) {
                                super.updateModuleConfiguration(module, rootModel)
                                val zeBuilder = builder.context.projectBuilder
                                if (zeBuilder is TrainxModuleBuilder) {
                                    zeBuilder.moduleCreated(module)
                                    println("We created a module: $module")
                                    return
                                }
                            }
                        })
                    }
                }
                projectDescriptor.modules = modules
            }
        }
        println("setupProjectStructure exit")
    }

    override fun getDetectorId() = "Trainx"

}
