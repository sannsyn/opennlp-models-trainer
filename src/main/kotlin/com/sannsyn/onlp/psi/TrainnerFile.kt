package com.sannsyn.onlp.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.psi.FileViewProvider
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.filetypes.trainner.TrainnerLanguage

class TrainnerFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, TrainnerLanguage) {

  private val trainnerFileType = TrainnerFileType()
  override fun getFileType() = trainnerFileType

  override fun toString() = "Trainner File"

}
