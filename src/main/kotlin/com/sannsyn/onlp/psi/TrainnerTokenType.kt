package com.sannsyn.onlp.psi

import com.intellij.psi.tree.IElementType
import com.sannsyn.onlp.filetypes.trainner.TrainnerLanguage
import org.jetbrains.annotations.*

class TrainnerTokenType(@NonNls debugName: String) : IElementType(debugName, TrainnerLanguage) {

  override fun toString(): String {
    return "TrainnerTokenType." + super.toString()
  }
}