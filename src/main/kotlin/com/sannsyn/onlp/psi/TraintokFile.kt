package com.sannsyn.onlp.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.psi.FileViewProvider
import com.sannsyn.onlp.filetypes.traintok.TraintokFileType
import com.sannsyn.onlp.filetypes.traintok.TraintokLanguage

class TraintokFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, TraintokLanguage) {

  private val traintokFileType = TraintokFileType()
  override fun getFileType() = traintokFileType

  override fun toString(): String {
    return "Traintok File"
  }

}
