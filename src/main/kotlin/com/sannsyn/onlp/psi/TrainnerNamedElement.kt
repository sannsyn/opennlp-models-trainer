package com.sannsyn.onlp.psi

import com.intellij.psi.PsiNameIdentifierOwner

interface TrainnerNamedElement : PsiNameIdentifierOwner
