package com.sannsyn.onlp.psi

import com.intellij.extapi.psi.PsiFileBase
import com.intellij.psi.FileViewProvider
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.filetypes.trainsent.TrainsentLanguage

class TrainsentFile(viewProvider: FileViewProvider) : PsiFileBase(viewProvider, TrainsentLanguage) {

  private val trainsentFileType = TrainsentFileType()
  override fun getFileType() = trainsentFileType

  override fun toString() = "Trainsent File"

}
