package com.sannsyn.onlp.psi

import com.intellij.psi.tree.IElementType
import com.sannsyn.onlp.filetypes.trainner.TrainnerLanguage
import org.jetbrains.annotations.*

class TrainnerElementType(@NonNls debugName: String) : IElementType(debugName, TrainnerLanguage)
