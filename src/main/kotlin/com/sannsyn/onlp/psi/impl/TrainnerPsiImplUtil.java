package com.sannsyn.onlp.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons;
import com.sannsyn.onlp.psi.TrainnerTaggedEntity;
import com.sannsyn.onlp.psi.TrainnerTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

// TrainnerTaggedEntity created by GrammarKit when interpreting/compiling the BNF file

class TrainnerPsiImplUtil {

    private TrainnerPsiImplUtil() { }

    @Nullable
    public static String getTagType(TrainnerTaggedEntity element) {
        @Nullable ASTNode child = element.getNode().findChildByType(TrainnerTypes.TAG_TYPE);
        return child == null ? null : child.getText();
    }

    @Nullable
    public static String getNamedEntityAsReadableText(TrainnerTaggedEntity element) {
        @Nullable ASTNode namedEntityNode = element.getNode().findChildByType(TrainnerTypes.NAMED_ENTITY);
        return namedEntityNode == null ? null : namedEntityNode.getText().replace("<SPLIT>", " ");
    }

    public static ItemPresentation getPresentation(TrainnerTaggedEntity element) {
        return new ItemPresentation() {
            @Override
            public /* @NlsSafe */ @Nullable String getPresentableText() {
                return element.getNamedEntityAsReadableText();
            }

            @Override
            public /* @NlsSafe */ @Nullable String getLocationString() {
                //return element.getNode().getElementType().toString();
                return element.getTagType();
            }

            @Override
            public @NotNull Icon getIcon(boolean unused) {
                return TrainxIcons.TRAINNER;
            }
        };
    }


}