package com.sannsyn.onlp.psi.impl

import com.intellij.extapi.psi.ASTWrapperPsiElement
import com.intellij.lang.ASTNode
import com.sannsyn.onlp.psi.TrainnerNamedElement

abstract class TrainnerNamedElementImpl(node: ASTNode) : ASTWrapperPsiElement(node), TrainnerNamedElement
