package com.sannsyn.onlp

import com.intellij.AbstractBundle
import org.jetbrains.annotations.NonNls
import org.jetbrains.annotations.PropertyKey
import java.io.FileNotFoundException
import java.lang.ref.SoftReference
import java.util.*

/**
 * See for instance
 * https://upsource.jetbrains.com/idea-ce/file/idea-ce-43dc757213a0b26e7b6ff28132d08c0782bc9a09/plugins/yaml/src/org/jetbrains/yaml/YAMLBundle.java
 */
object TrainxBundle {

    private var ourBundle = SoftReference<ResourceBundle>(null)
    @NonNls
    private const val BUNDLE = "localization.strings"

    private val bundle: ResourceBundle
        get() {
            var bundle = ourBundle.get()
            if (bundle == null) {
                bundle = ResourceBundle.getBundle(BUNDLE)
                ourBundle = SoftReference(bundle)
            }
            return bundle ?: throw FileNotFoundException("Couldn't find the resource bundle")
        }

    //@JvmOverloads
    fun message(@PropertyKey(resourceBundle = BUNDLE) key: String, vararg params: Any): String {
        return AbstractBundle.message(bundle, key, *params)
    }

}
