package com.sannsyn.onlp.errorhandling

import com.fasterxml.jackson.databind.ObjectMapper
import com.intellij.diagnostic.AbstractMessage
import com.intellij.ide.plugins.PluginManager
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.diagnostic.ErrorReportSubmitter
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.util.Consumer
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.project.TrainxProjectStartupActivity
import java.awt.Component
import java.net.HttpURLConnection
import java.net.URI
import java.util.*


class TrainxBbReportSubmitter : ErrorReportSubmitter() {

    override fun getReportActionText(): String {
        // https://bitbucket.org/dancioca/dbn/src/master/src/META-INF/plugin.xml
        // https://android.googlesource.com/platform/tools/adt/idea/+/refs/heads/mirror-goog-studio-master-dev/android/src/com/android/tools/idea/diagnostics/error/ErrorReporter.java
        return TrainxBundle.message("ErrorReporter.submitButtonText")
    }

    override fun submit(
                            events: Array<IdeaLoggingEvent?>,
                            additionalInfo: String?,
                            parentComponent: Component,
                            consumer: Consumer<in SubmittedReportInfo?> ): Boolean {

        if (events.isEmpty() || events[0] == null) {
            return false
        }

        val event = events[0]!!
        val data = event.data
        if (data is AbstractMessage) {
            val attachments = data.includedAttachments
        }

        val authToken = getAuthToken()

        // https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues#post
        // https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/issues
        val url = URI("https://api.bitbucket.org/2.0/repositories/sannsyn/opennlp-models-trainer/issues").toURL()
        val content = escapeJson(
                """$additionalInfo
                    Stacktrace:
                    ${event.throwableText}
                """.trimIndent()
        )

        var origin = event.throwable
        while (origin.cause != null) {
            origin = origin.cause
        }
        println("localizedMessage: " + origin.localizedMessage)
        val title = if (origin.localizedMessage == null || origin.localizedMessage == "null") {
            origin.javaClass.simpleName
        } else {
            origin.localizedMessage
        }

        val payload = """{
            "title": "$title",
            "status": "new",
            "kind": "bug",
            "reported_by": { "username": "anonymous", "application": "OpenNLP models builder Idea plugin" },
            "content": { "raw": "$content" }
        }"""

        val connection: HttpURLConnection? = url.openConnection() as? HttpURLConnection
        connection?.apply {
            requestMethod = "POST"
            setRequestProperty("Content-type", "application/json; charset=utf-8")
            setRequestProperty("Authorization", "Bearer $authToken")
            useCaches = false
            doInput = true
            doOutput = true
            outputStream?.write(payload.toByteArray(Charsets.UTF_8))
        }

        val responseCode = connection?.responseCode
        if (responseCode == null || responseCode >= 400) {
            //val errors = connection?.errorStream?.read()
            //val message = connection?.responseMessage
            //System.err.println("Response code: $responseCode,\nerrors: $errors,\nmessage: $message")
            //val x = connection?.errorStream?.readAllBytes()?.toString(Charsets.UTF_8)
            //System.err.println("Response: $x")
            return false
        }

        //val responseMessage = connection.responseMessage
        //println("Response message: $responseMessage")

        return true
    }

    private fun escapeJson(input: String): String {
        val stringBuilder = StringBuilder()
        for (char in input) {
            when (char) {
                '\"' -> stringBuilder.append("\\\"")
                '\\' -> stringBuilder.append("\\\\")
                '\b' -> stringBuilder.append("\\b")
                '\u000C' -> stringBuilder.append("\\f")
                '\n' -> stringBuilder.append("\\n")
                '\r' -> stringBuilder.append("\\r")
                '\t' -> stringBuilder.append("\\t")
                else -> {
                    if (char.isISOControl() || char !in '\u0020'..'\u007E') {
                        stringBuilder.append(String.format("\\u%04x", char.code))
                    } else {
                        stringBuilder.append(char)
                    }
                }
            }
        }
        return stringBuilder.toString()
    }
    // https://developer.atlassian.com/bitbucket/api/2/reference/meta/authentication
    private fun getAuthToken(): String {

        // get our version (for identifying ourselves):
        val runtimePluginId = PluginManager.getPluginByClass(TrainxProjectStartupActivity::class.java)?.pluginId
        val runtimePlugin = PluginManagerCore.getPlugin(runtimePluginId)
        val version = runtimePlugin?.version ?: "??"

        val authCon =
                URI("https://bitbucket.org/site/oauth2/access_token").toURL().openConnection() as? HttpURLConnection
        authCon?.apply {
            requestMethod = "POST"
            setRequestProperty("Authorization", "Basic $encodedBbAuthorization")
            setRequestProperty("User-Agent", "Idea-OpenNLP-Trainer-plugin/$version")
            instanceFollowRedirects = true
            useCaches = false
            doInput = true
            doOutput = true
            outputStream?.write("grant_type=client_credentials".toByteArray(Charsets.UTF_8))
            if (responseCode < 400) {
                val received = inputStream?.bufferedReader()?.readText() ?: "" // readText() should default to UTF-8
                val out = ObjectMapper().readTree(received)
                //println(out)
                return out["access_token"]?.asText() ?: ""
            } else {
                val errors = errorStream?.read()
                val message = responseMessage
                println("Response code for auth: $responseCode,\nerrors: $errors,\nmessage: $message\n")
                println("ERROR: " + (errorStream?.bufferedReader()?.readText() ?: ""))
            }
        }
        return ""
    }

    companion object {
        const val BB_KEY = "nhQ5sXhBf8RTwPdqwC"
        const val BB_SECRET = "Du2dJhh6UUafPY7tveecmNRCKCc5xfjd"
    }
}

val encodedBbAuthorization = Base64.getEncoder()
    .encode("${TrainxBbReportSubmitter.BB_KEY}:${TrainxBbReportSubmitter.BB_SECRET}".toByteArray()).toString(Charsets.UTF_8)