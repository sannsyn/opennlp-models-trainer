package com.sannsyn.onlp.actions.training

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.module.ModuleUtil
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.openapi.wm.ToolWindowManager
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.actions.TrainxOpenNlpUtils
import com.sannsyn.onlp.module.TrainxModuleBuilder
import com.sannsyn.onlp.module.TrainxModuleType
import com.sannsyn.onlp.ui.TrainxPanel
import com.sannsyn.onlp.ui.TrainxToolWindow
import opennlp.tools.dictionary.Dictionary
import opennlp.tools.sentdetect.SentenceModel
import java.time.LocalDateTime

/**
 * When this action is invoked, all .trainner files are compiled into one
 * long input stream, and a NER model is created / updated.
 */
class TrainSentenceModelAction : AnAction() {

    override fun actionPerformed(event: AnActionEvent) {

        // if we cannot find the project instance, just leave
        val project = event.project ?: return
        val baseDir = project.basePath?.run {
            LocalFileSystem.getInstance().findFileByPath(FileUtil.toSystemDependentName(this))
        }

        ApplicationManager.getApplication().runWriteAction {
            // creating folders must be done inside the runWriteAction() method.
            VfsUtil.createDirectoryIfMissing(baseDir, TrainxModuleBuilder.MODELS_FOLDER_NAME)
        }

        // Find the abbreviations dictionary (an xml file) in the /resources folder.
        // If no such folder / file is there, create it. The dictionary will in that
        // case be pre-populated with the string "<dictionary></dictionary>".
        var abbrevFile: VirtualFile? = null
        ApplicationManager.getApplication().runWriteAction {
            val resourcesDir = baseDir?.findFileByRelativePath(TrainxModuleBuilder.RESOURCES_FOLDER_NAME)
                    ?: baseDir?.createChildDirectory(this, TrainxModuleBuilder.RESOURCES_FOLDER_NAME)
            val abbrevDictFileName = TrainxOpenNlpUtils.getModelName("models.abbrevDict.filename", project)
            abbrevFile = resourcesDir?.findChild(abbrevDictFileName)
            if (abbrevFile == null) {
                abbrevFile = resourcesDir?.createChildData(this, abbrevDictFileName)
                abbrevFile?.setBinaryContent("<dictionary></dictionary>".toByteArray(Charsets.UTF_8))
            }
        } // end writeAction
        val abbrevDict = Dictionary(abbrevFile?.inputStream)

        // Then get hold of the model file:
        val compilerOutputDir = baseDir?.findFileByRelativePath(TrainxModuleBuilder.MODELS_FOLDER_NAME)
        val modelName = TrainxOpenNlpUtils.getModelName("models.sentencedetector.filename", project)
        var vFile = compilerOutputDir?.findChild(modelName)
        if (vFile == null) {
            ApplicationManager.getApplication().runWriteAction {
                vFile = compilerOutputDir?.createChildData(this, modelName)
            }
        }

        try {
            val twm = ToolWindowManager.getInstance(project)
            val toolWindow = twm.getToolWindow(TrainxToolWindow.TRAINX_TOOLWINDOW_ID) ?: return

            val contentManager = toolWindow.contentManager
            val panel = TrainxPanel()
            val content = contentManager.factory.createContent(panel, "Build output ${LocalDateTime.now().toLocalTime()}", true)
            contentManager.addContent(content)
            // TODO: use a progress monitor for this. Building a model may be time-consuming, especially when run with many iterations
            toolWindow.show {
                ApplicationManager.getApplication().executeOnPooledThread {
                    val sentenceModel: SentenceModel
                    try {
                        sentenceModel = TrainxOpenNlpUtils.buildSentenceModel(project, abbrevDict, panel)
                    } catch (exception: Exception) {
                        ApplicationManager.getApplication().invokeLater {
                            Messages.showMessageDialog(project,
                                    String.format(TrainxBundle.message("ModelBuildingException.message"), exception.localizedMessage),
                                    TrainxBundle.message("ModelBuildingException.title"),
                                    Messages.getWarningIcon())
                        }
                        return@executeOnPooledThread
                    }

                    ApplicationManager.getApplication().invokeLater {
                        sentenceModel.let { model ->
                            WriteAction.run<Exception> {
                                // overwriting
                                try {
                                    vFile?.getOutputStream(this)?.use {
                                        model.serialize(it)
                                    }
                                } catch (e: Exception) {
                                    System.err.println("Model NOT written!")
                                    throw e
                                }
                            }
                        }
                    }
                }
            }
            contentManager.setSelectedContent(content) // bring us to the foreground if we're not the only tab around here

        } catch (e: Exception) {
            System.err.println("Ew, it didn't work as expected: ${e.localizedMessage}")
            // TODO: alert user?
        }
    }

    override fun update(event: AnActionEvent) {
        val project = event.project
        event.presentation.isEnabledAndVisible =
                project != null &&
                        ModuleUtil.hasModulesOfType(project, TrainxModuleType())
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT

}

