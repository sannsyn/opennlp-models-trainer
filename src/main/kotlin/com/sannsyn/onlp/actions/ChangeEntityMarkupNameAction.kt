package com.sannsyn.onlp.actions

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.ui.Messages
import com.intellij.psi.util.PsiTreeUtil
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.project.askForAndSetDisplayedEntityName
import com.sannsyn.onlp.project.guessCurrentEntity
import com.sannsyn.onlp.project.setDisplayedEntityName
import com.sannsyn.onlp.psi.TrainnerParagraph
import com.sannsyn.onlp.psi.TrainnerTaggedEntity

/**
 * One might find that an entity has the wrong markup name.
 * This action is here to fix it.
 */
class ChangeEntityMarkupNameAction : AnAction() {

    /** This action is invoked when users right-click on a tagged entity to change its name */
    override fun actionPerformed(event: AnActionEvent) {
        val psiFile = event.getData(CommonDataKeys.PSI_FILE) ?: return

        println("You are right-clicking here: 1")
        if (psiFile.fileType !is TrainnerFileType) {
            return
        }

        println("You are right-clicking here: 2")
        val editor: Editor = event.getData(CommonDataKeys.EDITOR) ?: return

        println("You are right-clicking here: 3")
        val project = event.project ?: return

        println("You are right-clicking here: 4")
        val selectionModel = editor.selectionModel
        val start = selectionModel.selectionStart
        val end = selectionModel.selectionEnd

        val parent = PsiTreeUtil.getParentOfType(psiFile.findElementAt(start), TrainnerParagraph::class.java)
        if (parent == PsiTreeUtil.getParentOfType(psiFile.findElementAt(end), TrainnerParagraph::class.java)) {
            // Not spanning lines

            val taggedEntity = PsiTreeUtil.getParentOfType(psiFile.findElementAt(start), TrainnerTaggedEntity::class.java)

            if (taggedEntity != null) {

                // If we get here, we believe the click is within a tagged entity!
                editor.selectionModel.setSelection(taggedEntity.textOffset, taggedEntity.textOffset + taggedEntity.textLength)


                val currentEntity = Messages.showInputDialog(project,
                        "Please define the new entity name: ", // TODO: replace with resource string
                        "Change Entity Name", // TODO: replace with resource string
                        Messages.getQuestionIcon(),
                        taggedEntity.tagType,
                        null) ?: return
                setDisplayedEntityName(project, psiFile.virtualFile, currentEntity)

                WriteCommandAction.runWriteCommandAction(project) {
                    editor.document.replaceString(
                            taggedEntity.textOffset,
                            taggedEntity.textOffset + taggedEntity.textLength,
                            "<START:" + currentEntity + "> " + taggedEntity.namedEntityAsReadableText + " <END>")
                }

                return
            } // end changing markup

            askForAndSetDisplayedEntityName(project, psiFile.virtualFile, guessCurrentEntity(psiFile, project, true)
                    ?: "")

        } // end not spanning lines



        println("You are right-clicking here: ")
    }

    override fun update(event: AnActionEvent) {
        event.presentation.isVisible =
                event.project != null &&
                event.getData(CommonDataKeys.EDITOR) != null

        val psiFile = event.getData(CommonDataKeys.PSI_FILE)
        event.presentation.isEnabled =
                event.presentation.isVisible == true &&
                psiFile?.fileType is TrainnerFileType
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT

}