package com.sannsyn.onlp.actions.modelapplication

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileTypes.PlainTextFileType
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.psi.PsiFileFactory
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.module.TrainxModuleBuilder
import com.sannsyn.onlp.project.openNlpToolsJar
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.tokenize.SimpleTokenizer
import opennlp.tools.tokenize.Tokenizer
import opennlp.tools.tokenize.TokenizerME
import opennlp.tools.tokenize.TokenizerModel
import java.io.File

class ConvertToTraintokFileAction : AnAction() {

    override fun actionPerformed(event: AnActionEvent) {
        val project = event.project ?: return

        val sentenceModel = loadModel(project, "models.sentencedetector.filename", ::SentenceModel) ?: return
        val tokenModel = loadModel(project, "models.tokenizer.filename", ::TokenizerModel)
//        if (tokenModel == null) {
//            println("Found the token model to be absent. We shall have to use an OpenNLP builtin one.")
//        }

        if (!qc(sentenceModel, project) || (tokenModel != null && !qc(tokenModel, project))) {
            return
        }
        // We're good to go!
        //println("Models ok")

        val psiFile = event.getData(CommonDataKeys.PSI_FILE) ?: return
        //println("psiFile: $psiFile")

        val newFilename = psiFile.name.substringBeforeLast('.') + ".traintok"
        val tokenTrainerFile = File(psiFile.virtualFile?.parent?.canonicalPath, newFilename)
        if (tokenTrainerFile.exists()) {
            // warn user about not overwriting?
            return
        }

        val psiText = psiFile.text
        val originalText =
                if (psiFile.fileType is TrainsentFileType) {
                    psiText
                } else {
                    // must be a text file, so divide it into sentences first
                    val builder = StringBuilder(psiText)
                    for (span in SentenceDetectorME(sentenceModel).sentPosDetect(psiText).reversed()) {
                        // We need this clause to stay within the bounds:
                        if (span.end < psiFile.textLength) {
                            // then only act if there wasn't already a line break:
                            if (builder.substring(span.end, span.end + 1) != "\r" && builder.substring(span.end, span.end + 1) != "\n") {
                                builder.insert(span.end + 1, System.lineSeparator())
                            }
                        }
                    }
                    builder.toString()
                }

        val sentences: Array<String> = originalText.lines().toTypedArray()

        val tokenizer: Tokenizer =
                if (tokenModel == null) {
                    SimpleTokenizer.INSTANCE as Tokenizer
                } else {
                    TokenizerME(tokenModel)
                }
        println("We picked the $tokenizer tokenizer.")

        val doc = StringBuilder()
        sentences.forEach { sentence ->
            val tokens = tokenizer.tokenize(sentence).toList()
            doc.appendLine(tokens.joinToString(separator = " "))
        }

        WriteAction.run<Exception> {
            val newPsiFile = PsiFileFactory.getInstance(project).createFileFromText(newFilename, TrainsentFileType(), doc.toString())
            psiFile.containingDirectory.add(newPsiFile)
        }

        LocalFileSystem.getInstance().findFileByIoFile(tokenTrainerFile)?.let {
            FileEditorManager.getInstance(project).openFile(it, true)
        }

    }

    override fun update(event: AnActionEvent) {
        val psiFile = event.getData(CommonDataKeys.PSI_FILE)
        val baseDir = event.project?.basePath?.run {
            LocalFileSystem.getInstance().findFileByPath(FileUtil.toSystemDependentName(this))
        }

        // Visible for plaintext/trainsent/trainner files in the resources folder and the training files folder
        event.presentation.isVisible =
                event.project != null &&
                        (psiFile?.fileType == PlainTextFileType.INSTANCE ||
                                psiFile?.fileType is TrainnerFileType ||
                                psiFile?.fileType is TrainsentFileType) &&
                        (psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.TRAININGFILES_FOLDER_NAME) ||
                                psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.RESOURCES_FOLDER_NAME))

        // Enabled for files that are already visible if we have the right tools (installed libraries and generated models)
        val project = event.project
        event.presentation.isEnabled =
                event.presentation.isVisible &&
                        openNlpToolsJar() != null &&
                        project != null
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT

}
