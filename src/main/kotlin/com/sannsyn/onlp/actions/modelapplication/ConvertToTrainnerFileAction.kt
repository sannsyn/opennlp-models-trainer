package com.sannsyn.onlp.actions.modelapplication

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileTypes.PlainTextFileType
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.psi.PsiFileFactory
import com.sannsyn.onlp.actions.TrainxOpenNlpUtils
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.filetypes.traintok.TraintokFileType
import com.sannsyn.onlp.module.TrainxModuleBuilder
import com.sannsyn.onlp.project.openNlpToolsJar
import opennlp.tools.namefind.NameFinderME
import opennlp.tools.namefind.TokenNameFinderModel
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.tokenize.*
import java.io.File

class ConvertToTrainnerFileAction : AnAction() {

    override fun actionPerformed(event: AnActionEvent) {
        val project = event.project ?: return

        // Ask user which type of named entities should be found?

        // TODO: alert user here if any of these are null?
        val nerModel = loadModel(project, "models.ner.filename", ::TokenNameFinderModel) ?: return
        val sentenceModel = loadModel(project, "models.sentencedetector.filename", ::SentenceModel) ?: return

        val tokenModel = loadModel(project, "models.tokenizer.filename", ::TokenizerModel)
        if (tokenModel == null) {
            println("Found the token model to be absent. We shall have to use an OpenNLP builtin one.")
        }

        if (!qc(nerModel, project) || !qc(sentenceModel, project) /*|| !qc(tokenModel, project) */) {
            return
        }
        // Sentence model and NER model exist, we're good to go!
        //println("All models OK")

        val psiFile = event.getData(CommonDataKeys.PSI_FILE) ?: return
        //println("psiFile: $psiFile")

        val newFilename = psiFile.name.substringBeforeLast('.') + ".trainner"
        val sentFile = File(psiFile.virtualFile?.parent?.canonicalPath, newFilename)
        if (sentFile.exists()) {
            // warn user about not overwriting?
            return
        }

        val psiText = psiFile.text
        val originalText =
                if (psiFile.fileType is TrainsentFileType || psiFile.fileType is TraintokFileType) {
                    psiText
                } else {
                    // must be a text file, so divide it into sentences first
                    val builder = StringBuilder(psiText)
                    for (span in SentenceDetectorME(sentenceModel).sentPosDetect(psiText).reversed()) {
                        // We need this clause to stay within the bounds:
                        if (span.end < psiFile.textLength) {
                            // then only act if there wasn't already a line break:
                            if (builder.substring(span.end, span.end + 1) != "\r" && builder.substring(span.end, span.end + 1) != "\n") {
                                builder.insert(span.end + 1, System.lineSeparator())
                            }
                        }
                    }
                    builder.toString()
                }

        val sentences: Array<String> = originalText.lines().toTypedArray()

//        AT THE MOMENT, WE KEEP NO EMPTY LINES ...
        println("We found ${sentences.size} sentences in the $psiFile")

        val nameFinder = NameFinderME(nerModel)
        val tokenizer: Tokenizer =
                when {
                    psiFile.fileType is TraintokFileType -> WhitespaceTokenizer.INSTANCE // probably is already tokenized
                    tokenModel == null                   -> SimpleTokenizer.INSTANCE as Tokenizer
                    else                                 -> TokenizerME(tokenModel)
                }
        println("We picked the $tokenizer tokenizer.")

        val doc = StringBuilder()
        sentences.forEach { sentence ->
            val tokens = tokenizer.tokenize(sentence)
            val nameSpans = nameFinder.find(tokens)

            if (nameSpans.isEmpty()) {
                // no names on this line, so simply concatenate:
                doc.appendLine(tokens.toList().joinToString(separator = " ")) // cannot use the 'sentence' val, since we want it as split-apart tokens
            } else {
                val words: MutableList<String> = mutableListOf()
                tokens.forEachIndexed { i, token ->
                    println("Index: $i")
                    nameSpans.forEach { span ->
                        if (span.start == i) {
                            words.add("<START:" + span.type + ">")
                        }
                        if (span.end == i) {
                            words.add("<END>")
                        }
                    }
                    words.add(token)
                }
                nameSpans.forEach { span ->
                    if (span.end == tokens.size) {
                        words.add("<END>")
                    }
                }

                doc.appendLine(words.joinToString(separator = " "))
            }

//                println("nameSpans (length ${nameSpans.size}) from ${tokens}:")
//                nameSpans.forEach {
//                    // This is where we should apply markup ...
//                    println("nameSpan: " + tokens.slice(it.start..it.end))
//                }
        } // end forEach sentence


        WriteAction.run<Exception> {
            val newPsiFile = PsiFileFactory.getInstance(project).createFileFromText(newFilename, TrainsentFileType(), doc.toString())
            psiFile.containingDirectory.add(newPsiFile)
        }

        LocalFileSystem.getInstance().findFileByIoFile(sentFile)?.let {
            FileEditorManager.getInstance(project).openFile(it, true)
        }

    }

    override fun update(event: AnActionEvent) {
        val psiFile = event.getData(CommonDataKeys.PSI_FILE)
        val baseDir = event.project?.basePath?.run {
            LocalFileSystem.getInstance().findFileByPath(FileUtil.toSystemDependentName(this))
        }

        // Visible for plaintext/trainsent/traintok files in the resources folder and the training files folder
        event.presentation.isVisible =
                event.project != null &&
                        (psiFile?.fileType == PlainTextFileType.INSTANCE ||
                                psiFile?.fileType is TraintokFileType ||
                                psiFile?.fileType is TrainsentFileType) &&
                (psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.TRAININGFILES_FOLDER_NAME) ||
                        psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.RESOURCES_FOLDER_NAME))

        // Enabled for files that are already visible if we have the right tools (installed libraries and generated models)
        val project = event.project
        event.presentation.isEnabled =
                event.presentation.isVisible &&
                openNlpToolsJar() != null &&
                project != null &&
                baseDir?.findFileByRelativePath(TrainxModuleBuilder.MODELS_FOLDER_NAME)?.findChild(TrainxOpenNlpUtils.getModelName("models.ner.filename", project)) != null
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT
}
