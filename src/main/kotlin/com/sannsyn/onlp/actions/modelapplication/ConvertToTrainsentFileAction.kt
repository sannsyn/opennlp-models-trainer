package com.sannsyn.onlp.actions.modelapplication

import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.fileTypes.PlainTextFileType
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.sannsyn.onlp.actions.TrainxOpenNlpUtils
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.module.TrainxModuleBuilder
import com.sannsyn.onlp.project.openNlpToolsJar
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.sentdetect.SentenceModel
import java.io.File


class ConvertToTrainsentFileAction : AnAction() {

    override fun actionPerformed(event: AnActionEvent) {
        val project = event.project ?: return

        val sentenceModel = loadModel(project, "models.sentencedetector.filename", ::SentenceModel) ?: return

        if (!qc(sentenceModel, project)) {
            return
        }
        // Sentence model exists, we're good to go!

        val psiFile: PsiFile = event.getData(CommonDataKeys.PSI_FILE) ?: return

        val newFilename = psiFile.name.substringBeforeLast('.') + ".trainsent"
        val sentFile = File(psiFile.virtualFile?.parent?.canonicalPath, newFilename)
        if (sentFile.exists()) {
            // warn user about not overwriting?
            return
        }

        val originalText = psiFile.text
        val detector = SentenceDetectorME(sentenceModel)
        val builder = StringBuilder(originalText)

        // while developing and debugging:
        val sentenceSpans = detector.sentPosDetect(originalText)
//        val probabilities = detector.sentenceProbabilities
//        for (i in 0 until sentenceSpans.size) {
//            println("Sentence $i: " + probabilities[i])
//        }
        // end while developing and debugging

        // when iterating, start at the end and go backwards, since we change the indices when inserting line breaks
        for (span in sentenceSpans.reversed()) {
            // We need this clause to stay within the bounds:
            if (span.end < psiFile.textLength) {
                // then only act if there wasn't already a line break:
                if (builder.substring(span.end, span.end+1) != "\r" && builder.substring(span.end, span.end+1) != "\n") {
                    builder.insert(span.end+1, System.lineSeparator())
                }
            }
        }

        WriteAction.run<Exception> {
            val newPsiFile = PsiFileFactory.getInstance(project).createFileFromText(newFilename, TrainsentFileType(), builder.toString())
            psiFile.containingDirectory.add(newPsiFile)
        }

        println("Just created $sentFile")
        LocalFileSystem.getInstance().findFileByIoFile(sentFile)?.let {
            FileEditorManager.getInstance(project).openFile(it, true)
        }
    }

    override fun update(event: AnActionEvent) {
        val psiFile = event.getData(CommonDataKeys.PSI_FILE)
        val baseDir = event.project?.basePath?.run {
            LocalFileSystem.getInstance().findFileByPath(FileUtil.toSystemDependentName(this))
        }

        event.presentation.isVisible =
                event.project != null &&
                psiFile?.fileType == PlainTextFileType.INSTANCE &&
                (psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.TRAININGFILES_FOLDER_NAME) ||
                        psiFile?.containingDirectory?.virtualFile == baseDir?.findFileByRelativePath(TrainxModuleBuilder.RESOURCES_FOLDER_NAME))

        val project = event.project
        event.presentation.isEnabled =
                event.presentation.isVisible &&
                openNlpToolsJar() != null &&
                project != null &&
                baseDir?.findFileByRelativePath(TrainxModuleBuilder.MODELS_FOLDER_NAME)?.findChild(TrainxOpenNlpUtils.getModelName("models.sentencedetector.filename", project)) != null
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT
}
