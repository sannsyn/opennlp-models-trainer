package com.sannsyn.onlp.actions.modelapplication

import com.intellij.openapi.project.Project
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.actions.TrainxOpenNlpUtils
import com.sannsyn.onlp.module.TrainxModuleBuilder
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager
import opennlp.tools.util.model.BaseModel
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Paths

fun <T : BaseModel> loadModel(project: Project, modelFileKey: String, fCall: (stream: InputStream) -> T): T? {
    val baseDir = project.basePath?.run {
        LocalFileSystem.getInstance().findFileByPath(FileUtil.toSystemDependentName(this))
    }

    val compilerOutputDir = baseDir?.findFileByRelativePath(TrainxModuleBuilder.MODELS_FOLDER_NAME)
    // TODO1: give some useful feedback to user when we return here because of missing models file
    // TODO2: before converting to a traintok file, we should remove all .trainner file markup from the text ...!
    val vFile = compilerOutputDir?.findChild(TrainxOpenNlpUtils.getModelName(modelFileKey, project)) ?: return null

    return if (vFile.exists() && vFile.length > 0) {
        Files.newInputStream(Paths.get(vFile.path)).use { fCall(it) }
    } else {
        null
    }
}


fun qc(model: BaseModel, project: Project): Boolean {
    // TODO: conclusions are drawn in this function, but we might
    //       consider involving the user a bit ...

    if (model.language != TrainxProjectSettingsManager.getInstance(project)?.state?.language) {
        println(
                // TODO: maybe display this to user?
                String.format(TrainxBundle.message("models.wrongLanguageWarning"),
                        /* 1: */ model.getManifestProperty("Component-Name"),
                        /* 2: */ model.language,
                        /* 3: */ TrainxProjectSettingsManager.getInstance(project)?.state?.language
                )
        )
        return false
    }



    return true

}

