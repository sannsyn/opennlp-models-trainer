package com.sannsyn.onlp.actions

import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiWhiteSpace
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.ui.components.JBLabel
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.project.guessCurrentEntity
import com.sannsyn.onlp.project.setDisplayedEntityName
import com.sannsyn.onlp.psi.TrainnerParagraph
import com.sannsyn.onlp.psi.TrainnerTaggedEntity
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager


/**
 * This actually is the main purpose of this whole plugin:
 * double-clicking a word applies OpenNLP markup to it
 */
class OneClickEntityMarkupAction : AnAction() {

    override fun actionPerformed(event: AnActionEvent) {
        val project = event.project ?: return

        if (TrainxProjectSettingsManager.getInstance(project)?.state?.mouseClickMarkupMode == false) {
            return
        }

        val psiFile = event.getData(CommonDataKeys.PSI_FILE) ?: return

        if (psiFile.fileType !is TrainnerFileType) {
            return
        }

        if (event.dataContext.getData(PlatformDataKeys.CONTEXT_COMPONENT) is JBLabel) {
            // Probably clicking on the top bar to change current markup entity name
            return
        }

        val editor: Editor = event.getData(CommonDataKeys.EDITOR) ?: return

        // http://www.jetbrains.org/intellij/sdk/docs/basics/architectural_overview/documents.html?search=getEventMulticaster
        // says:
        // If the file corresponding to a Document is read-only (for example, not checked out from the version control system),
        // document modifications will fail. Thus, before modifying the Document, it is necessary to call
        // ReadonlyStatusHandler.getInstance(project).ensureFilesWritable() to check out the file if necessary.

        // AND THE NEXT THING WE DO:
        // http://www.jetbrains.org/intellij/sdk/docs/tutorials/editor_basics/editor_events.html?search=editoractionhandler
        // HAS A SECTION ABOUT MAKING A NEW TypedHandler. MAYBE THAT COULD BE USED FOR EditorActionHandlers TOO?
        // https://upsource.jetbrains.com/idea-ce/file/idea-ce-ebcc6906bd82905d36f7f0523c81087dbb3a5284/platform/platform-api/src/com/intellij/openapi/editor/actionSystem/EditorAction.java

        toggleMarkup(psiFile, project, editor)

        // https://www.jetbrains.org/intellij/sdk/docs/tutorials/editor_basics/working_with_text.html

    }

    override fun update(event: AnActionEvent) {
        event.presentation.isVisible =
                event.getData(CommonDataKeys.EDITOR) != null

        val psiFile = event.getData(CommonDataKeys.PSI_FILE)
        event.presentation.isEnabled =
                event.presentation.isVisible == true &&
                psiFile?.fileType is TrainnerFileType
    }

    override fun getActionUpdateThread() = ActionUpdateThread.BGT

    companion object {

        /** Toggles the named entity tagging. */
        fun toggleMarkup(psiFile: PsiFile, project: Project, editor: Editor) {

            val selectionModel = editor.selectionModel
            var start = selectionModel.selectionStart
            var end = selectionModel.selectionEnd

            if (start == 0 && end == psiFile.text.length) {
                // When doubleClicking outside a word (on whitespace), the whole document is selected,
                // and we do not want to mark up the whole document.
                return
            }

            val parent = PsiTreeUtil.getParentOfType(psiFile.findElementAt(start), TrainnerParagraph::class.java)
            if (parent != PsiTreeUtil.getParentOfType(psiFile.findElementAt(end), TrainnerParagraph::class.java)) {
                // Cannot do markup spanning lines
                return
            }

            val taggedEntity = PsiTreeUtil.getParentOfType(psiFile.findElementAt(start), TrainnerTaggedEntity::class.java)
            if (taggedEntity == null) {

                // expand selection to word boundaries:
                while (start > 0 &&
                        psiFile.findElementAt(start-1) !is PsiWhiteSpace &&
                        psiFile.findElementAt(start-1)?.parent == parent) {
                    start--
                }
                while (end < psiFile.text.length &&
                        psiFile.findElementAt(end) !is PsiWhiteSpace &&
                        psiFile.findElementAt(end+1)?.parent == parent) {
                    end++
                }

                // ... but do we CONTAIN one?
                if (psiFile.findElementAt(start)?.children?.none() == false) {
//                    println("We're not supposed to contain any markup when we click ... So returning ...")
                    return
                }

                editor.selectionModel.setSelection(start, end)

                editor.selectionModel.selectedText ?: return // empty selection

                val currentEntity = guessCurrentEntity(psiFile, project, true) ?: return
                setDisplayedEntityName(project, psiFile.virtualFile, currentEntity)

                WriteCommandAction.runWriteCommandAction(project) {
                    editor.document.replaceString(start, end, "<START:$currentEntity> " + editor.selectionModel.selectedText + " <END>")
                }

                println("End adding markup")
                return
            } // end adding markup

            // If we get this far, we believe the click is within a tagged entity!
            println("Click within tagged entity")

            editor.selectionModel.setSelection(taggedEntity.textOffset, taggedEntity.textOffset + taggedEntity.textLength)

                WriteCommandAction.runWriteCommandAction(project) {
                    editor.document.replaceString(
                            taggedEntity.textOffset,
                            taggedEntity.textOffset + taggedEntity.textLength,
                            taggedEntity.namedEntityAsReadableText ?: "")
                }

        }

    }

}


