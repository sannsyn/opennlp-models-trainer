package com.sannsyn.onlp.actions

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.fileTypes.LanguageFileType
import com.intellij.openapi.module.Module
import com.intellij.openapi.module.ModuleManager
import com.intellij.openapi.module.ModuleType
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.Messages
import com.intellij.psi.search.FilenameIndex
import com.intellij.psi.search.GlobalSearchScope
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.filetypes.trainner.TrainnerFileType
import com.sannsyn.onlp.filetypes.trainsent.TrainsentFileType
import com.sannsyn.onlp.filetypes.traintok.TraintokFileType
import com.sannsyn.onlp.module.TrainxModuleType
import com.sannsyn.onlp.settings.TrainxProjectSettings
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager
import com.sannsyn.onlp.ui.TrainxPanel
import opennlp.tools.dictionary.Dictionary
import opennlp.tools.namefind.NameFinderME
import opennlp.tools.namefind.NameSampleDataStream
import opennlp.tools.namefind.TokenNameFinderFactory
import opennlp.tools.namefind.TokenNameFinderModel
import opennlp.tools.sentdetect.SentenceDetectorFactory
import opennlp.tools.sentdetect.SentenceDetectorME
import opennlp.tools.sentdetect.SentenceModel
import opennlp.tools.sentdetect.SentenceSampleStream
import opennlp.tools.tokenize.TokenSampleStream
import opennlp.tools.tokenize.TokenizerFactory
import opennlp.tools.tokenize.TokenizerME
import opennlp.tools.tokenize.TokenizerModel
import opennlp.tools.util.PlainTextByLineStream
import opennlp.tools.util.TrainingParameters
import java.io.*
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

internal object TrainxOpenNlpUtils {

    private const val COULD_NOT_FIND_THE_OPENNLP_TRAINER_MODULE = "Couldn't find the OpenNLP Trainer Module!"
    private const val MODEL_BUILT = "\nModel built.\n"

    internal fun buildNEModel(project: Project, capturePanel: TrainxPanel): TokenNameFinderModel {
        //println("OnlpModelBuildingUtil.buildNEModel enter. Current project is ${project}")
        val settings = qcSettings(project)

        val parameters = TrainingParameters.defaultParams()
        parameters.put(TrainingParameters.ITERATIONS_PARAM, settings.noOfTrainingIterations)
        //parameters.put(TrainingParameters.ITERATIONS_PARAM, Integer.toString(100))
        //parameters.put(TrainingParameters.CUTOFF_PARAM, Integer.toString(3));
        //parameters.put(TrainingParameters.ITERATIONS_PARAM, 2000) // TODO: transform number of repetitions to a setting somewhere
        // 20000 iterasjoner gjør bl.a. at forkortelsen "fork." i begynnelsen av en
        // linje blir behandlet riktig. Det gjør den ikke med 100 iterasjoner (som er standard) ...

        // The gen/models folder is supposed to be among the source roots:
        //val modelsFolder = File(project.basePath, TrainxModuleBuilder.MODELS_FOLDER_NAME)
        //println("                               Models folder should be ${modelsFolder}")
        //val modelfile = File(modelsFolder, TrainxOpenNlpUtils.nerModelName(project))
        //println("                               Our previous model file: ${modelfile}")
        //val modelfactory = if (modelfile.exists()) FileInputStream(modelfile).use { TokenNameFinderModel(it) }.factory else TokenNameFinderFactory()
        val modelfactory = TokenNameFinderFactory()
        //println("                               Model factory: ${modelfactory}")

        val myModule = getModule(project) ?: throw IllegalStateException(COULD_NOT_FIND_THE_OPENNLP_TRAINER_MODULE)

        val inputstreamFactory = createInputStreamFactoryForFiles("trainner", TrainnerFileType(), project, myModule)

        //val jarFile = TrainxProjectStartupActivity.openNlpToolsJar()

        // IMPORTANT: Save the old System.out!
        val old = System.out
        System.setOut(PrintStream(StreamCapturer(capturePanel)))

        val lineStream = PlainTextByLineStream(inputstreamFactory, StandardCharsets.UTF_8)

        val toReturn = NameSampleDataStream(lineStream).use {
            NameFinderME.train(settings.language, "person", it, parameters, modelfactory)
        }

        println(MODEL_BUILT) // terminating with \n assures everything is flushed to the TrainxPanel in the StreamCapturer
        System.setOut(old)

        return toReturn //?: throw Exception("Model couldn't be built.")

    }

    @Throws(FileNotFoundException::class, IOException::class)
    internal fun buildSentenceModel(project: Project, abbrevDict: Dictionary, capturePanel: TrainxPanel): SentenceModel {
        //println("TrainxOnlpUtils.buildSentenceModel enter. Current project is $project")

        // First, let's prepare the stuff we need for training to take place:
        val myModule = getModule(project) ?: throw IllegalStateException(COULD_NOT_FIND_THE_OPENNLP_TRAINER_MODULE)
        // get an input stream factory for trainsent files
        val inputStreamFactory = createInputStreamFactoryForFiles("trainsent", TrainsentFileType(), project, myModule)
        val settings = qcSettings(project)

        // Prepare the parameters defined in settings
        val parameters = TrainingParameters.defaultParams()

        // First the number of iterations in the model build process:
        parameters.put(TrainingParameters.ITERATIONS_PARAM, settings.noOfTrainingIterations)
        // Then the end-of-sentence characters:
        val eosCharacters = settings.eosCharacters.toCharArray()

        // Then construct the factory class:
        val factory = SentenceDetectorFactory(settings.language, true, abbrevDict, eosCharacters)

        // and we're good to go ...
        println("parameters: $parameters")

        val old = System.out
        System.setOut(PrintStream(StreamCapturer(capturePanel)))

        val lineStream = PlainTextByLineStream(inputStreamFactory, StandardCharsets.UTF_8)

        val toReturn = SentenceSampleStream(lineStream).use {
            SentenceDetectorME.train(settings.language, it, factory, parameters)
        }

        println(MODEL_BUILT) // terminating with \n assures everything is flushed to the TrainxPanel in the StreamCapturer
        System.setOut(old)

        return toReturn //?: throw Exception("Model couldn't be built.")

    }

    @Throws(FileNotFoundException::class, IOException::class)
    internal fun buildTokenizerModel(project: Project, abbrevDict: Dictionary, capturePanel: TrainxPanel): TokenizerModel {
        //println("TrainxOnlpUtils.buildTokenizerModel enter. Current project is $project")

        // First, let's prepare the stuff we need for training to take place:
        val myModule = getModule(project) ?: throw IllegalStateException(COULD_NOT_FIND_THE_OPENNLP_TRAINER_MODULE)
        // get an inputstream factory for traintok files
        val inputstreamFactory = createInputStreamFactoryForFiles("traintok", TraintokFileType(), project, myModule)
        val settings = qcSettings(project)

        // Prepare the parameters defined in settings
        val parameters = TrainingParameters.defaultParams()

        // First the number of iterations in the model build process:
        parameters.put(TrainingParameters.ITERATIONS_PARAM, settings.noOfTrainingIterations)
        println("Number of iterations: " + settings.noOfTrainingIterations)

        // Then construct the factory class:
        val factory = TokenizerFactory(settings.language, abbrevDict, false, null)

        // and we're good to go ...

        val old = System.out
        System.setOut(PrintStream(StreamCapturer(capturePanel)))

        try {

            val lineStream = PlainTextByLineStream(inputstreamFactory, StandardCharsets.UTF_8)

            val toReturn = TokenSampleStream(lineStream).use {
                TokenizerME.train(it, factory, parameters)
            }

            println(MODEL_BUILT) // terminating with \n assures everything is flushed to the TrainxPanel in the StreamCapturer

            return toReturn //?: throw Exception("Model couldn't be built.")

        } finally {
            System.setOut(old)
        }

    }

    /**
     * Retrieve the settings for this project. The «quality control» implies
     * that we see to it that needed parameters are actually set.
     */
    private fun qcSettings(project: Project): TrainxProjectSettings {
        val trainxProjectSettings = TrainxProjectSettingsManager.getInstance(project)?.state ?: TrainxProjectSettings()

        if (trainxProjectSettings.language.isBlank()) {

            ApplicationManager.getApplication().invokeAndWait {
                while (trainxProjectSettings.language.isBlank()) {
                    trainxProjectSettings.language = Messages.showInputDialog(project,
                            TrainxBundle.message("settingsManagement.noRegisteredLanguage.inputFieldPresentation"),
                            TrainxBundle.message("settingsManagement.noRegisteredLanguage.dialogHeading"),
                            Messages.getQuestionIcon()) ?: ""
                }
            }
        }
        return trainxProjectSettings
    }

    fun getModelName(modelNameKey: String, project: Project): String =
            TrainxProjectSettingsManager.getInstance(project)?.state?.language + TrainxBundle.message(modelNameKey)

    /**
     * Returns a factory that can create an inputstream from a given project and module by
     * retrieving all files with the specified extension and passing them into a chain.
     *
     * The [extension] is given without the dot.
     */
    private fun createInputStreamFactoryForFiles(extension: String, fileType: LanguageFileType, project: Project, module: Module): () -> SequenceInputStream {

        return {

            var toReturn: SequenceInputStream? = null

            ApplicationManager.getApplication().runReadAction {
                val files = FilenameIndex.getAllFilesByExt(project, extension, GlobalSearchScope.moduleScope(module))
                println("Found " + files.size + " files with the ." + extension + " extension ...")

                toReturn = SequenceInputStream(
                    Collections.enumeration(
                                files
                                        .filter {
                                            // Seems the object (!) TrainsentFileType exists as more than one instantiation
                                            // Check if this is the case later.
                                            // If we're back to only one instance of the object, we can go back to
                                            // checking      it.fileType == fileType
                                            it.fileType.defaultExtension == fileType.defaultExtension
                                        }
                                        .map { Files.newInputStream(Paths.get(it.path)) }
                                        .toSet()
                        )
                )
            }
            toReturn ?: throw IOException("Couldn't read project files")
        }

    }

    private fun getModule(project: Project): Module? =
        ModuleManager.getInstanceIfDefined(project)?.modules?.firstOrNull {
            TrainxModuleType().name == ModuleType.get(it).name
        }

}


class StreamCapturer(private val capturePane: TrainxPanel) : OutputStream() {
    // https://stackoverflow.com/a/12945678

    private val buffer: StringBuilder = StringBuilder(128)

    override fun write(b: Int) {
        val character = b.toChar()
        buffer.append(character)
        if (character == '\n') {
            capturePane.appendText(buffer.toString())
            buffer.delete(0, buffer.length)
        }
    }
}