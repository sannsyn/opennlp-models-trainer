package com.sannsyn.onlp.actions

import com.intellij.ide.plugins.PluginManager
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.ui.Messages
import com.sannsyn.onlp.TrainxBundle


class AboutAction : AnAction() {

    /**
     * Implement this method to provide your action handler.
     * @param event Carries information on the invocation place
     */
    override fun actionPerformed(event: AnActionEvent) {

        val project = event.project ?: return

        // get our version:
        val runtimePluginId = PluginManager.getPluginByClass(com.sannsyn.onlp.project.TrainxProjectStartupActivity::class.java)?.pluginId
        val runtimePlugin = PluginManagerCore.getPlugin(runtimePluginId)
        val version = runtimePlugin?.version ?: "??"

        @Suppress("DialogTitleCapitalization")
        Messages.showMessageDialog(project,
                String.format(TrainxBundle.message("action.com.sannsyn.onlp.actions.AboutAction.messageHtml"),
                        /* 1: */ version,
                        /* 2: */ TrainxBundle.message("action.com.sannsyn.onlp.actions.AboutAction.projectLink")),
                TrainxBundle.message("action.com.sannsyn.onlp.actions.AboutAction.messageTitle"),
                Messages.getInformationIcon())
    }
}
