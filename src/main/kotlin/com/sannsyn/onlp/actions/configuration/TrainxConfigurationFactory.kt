package com.sannsyn.onlp.actions.configuration

import com.intellij.execution.configurations.ConfigurationFactory
import com.intellij.execution.configurations.ConfigurationType
import com.intellij.execution.configurations.RunConfiguration
import com.intellij.openapi.project.Project

class TrainxConfigurationFactory(type: ConfigurationType) : ConfigurationFactory(type) {

    /**
     * Creates a new template run configuration within the context of the specified project.
     *
     * @param project the project in which the run configuration will be used
     * @return the run configuration instance.
     */
    override fun createTemplateConfiguration(project: Project): RunConfiguration {
        return TrainxConfiguration(project, this, "Onlp Model Building Configuration")
    }
}