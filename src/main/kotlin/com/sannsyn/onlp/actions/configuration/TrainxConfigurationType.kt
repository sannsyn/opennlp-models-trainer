package com.sannsyn.onlp.actions.configuration

import com.intellij.execution.configurations.ConfigurationTypeBase
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

class TrainxConfigurationType :
        ConfigurationTypeBase("com.sannsyn.onlp.actions.modelbuilding.TrainxConfigurationType", "OpenNLP", "Build an OpenNLP model", TrainxIcons.TRAINX) {

    init {
        addFactory(TrainxConfigurationFactory(this))
    }

}