package com.sannsyn.onlp.actions

import com.intellij.openapi.actionSystem.DefaultActionGroup
import com.sannsyn.onlp.TrainxBundle

/**
 * This class is here to assure that the group of Trainx tools available in the editor popup menu
 * is only visible when users click in a .trainx document.
 */
class TrainnerEditorPopupMenuGroup: DefaultActionGroup() {

    init {
        templatePresentation.text = TrainxBundle.message("menugroup.text")
        templatePresentation.description = TrainxBundle.message("menugroup.description")
    }

}