package com.sannsyn.onlp.module

import org.jetbrains.jps.model.ex.JpsElementTypeBase
import org.jetbrains.jps.model.module.JpsModuleSourceRootType

/**
 * Adapted from the Java source root type class
 */
class TrainxSourceRootType private constructor() : JpsElementTypeBase<TrainxSourceRootProperties>(), JpsModuleSourceRootType<TrainxSourceRootProperties> {

    override fun createDefaultProperties(): TrainxSourceRootProperties {
        return TrainxSourceRootProperties()
    }

    companion object {
        val TRAINX_SOURCE = TrainxSourceRootType()
        val TEXT_SOURCE = TrainxSourceRootType()
        val MODELS = TrainxSourceRootType() // Models are both "generated sources" and end products at the same time ...
        val TEST_SOURCE = TrainxSourceRootType()
    }
}
