package com.sannsyn.onlp.module

// https://plugins.jetbrains.com/docs/intellij/project-wizard.html#implementing-module-builder

import com.intellij.ide.util.projectWizard.*
import com.intellij.openapi.module.Module
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.roots.ModifiableRootModel
import com.intellij.openapi.roots.ModuleRootManager
import com.intellij.openapi.roots.ui.configuration.ModulesProvider
import com.intellij.openapi.util.Pair
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.sannsyn.onlp.TrainxBundle
import com.sannsyn.onlp.settings.TrainxProjectSettings
import com.sannsyn.onlp.settings.TrainxProjectSettingsManager
import com.sannsyn.onlp.settings.TrainxProjectSettingsPanel
import org.jetbrains.annotations.NonNls
import java.io.File
import javax.swing.JComponent


/**
 * Inspired / adapted from the JavaModuleBuilder
 */
class TrainxModuleBuilder : ModuleBuilder(), SourcePathsBuilder, ModuleBuilderListener {

    private val mySourcePaths: MutableList<Pair<String, String>> by lazy {
        mutableListOf<Pair<String, String>>().apply {
            val path1 = contentEntryPath + File.separator + TRAININGFILES_FOLDER_NAME
            val path2 = contentEntryPath + File.separator + RESOURCES_FOLDER_NAME
            val path3 = contentEntryPath + File.separator + MODELS_FOLDER_NAME

            File(path1).mkdirs()
            File(path2).mkdirs()
            //File(path3).mkdirs()

            add(Pair.create(path1, "")) // the empty string is the «package prefix for the root to add»
            add(Pair.create(path2, ""))
            add(Pair.create(path3, ""))
        }
    }

    init {
        addListener(this)
    }

    // SourcePathsBuilder asks us for five methods. Two of these are implemented by
    // the generic ModuleBuilder that we extend. The other three are these:
    override fun getSourcePaths() = mySourcePaths
    override fun setSourcePaths(sourcePaths: List<Pair<String, String>>) {
        mySourcePaths.clear()
        mySourcePaths.addAll(sourcePaths)
    }
    override fun addSourcePath(sourcePathInfo: Pair<String, String>) {
        mySourcePaths.add(sourcePathInfo)
    }


    private val myType = TrainxModuleType()

    // ModuleBuilder abstract method
    override fun getModuleType() = myType

    override fun getBuilderId(): String? = this.javaClass.name

    override fun getPresentableName() = TrainxBundle.message("ModuleBuilder.PresentableName")

    override fun getGroupName() = builderId

    // http://www.jetbrains.org/intellij/sdk/docs/reference_guide/project_wizard.html#adding-new-wizard-steps
    override fun createWizardSteps(wizardContext: WizardContext, modulesProvider: ModulesProvider): Array<ModuleWizardStep> {
        return arrayOf(wizStep)
    }

    //TrainxProjectSettingsManager.getInstance(project)?.state?.language + TrainxBundle.message(modelNameKey)
    override fun getDescription() = TrainxBundle.message("ModuleBuilder.Description")

    override fun moduleCreated(module: Module) {
        val contentRoots = ModuleRootManager.getInstance(module).contentRoots
        println("New OpenNLP Models Trainer Module Project Created. Content roots: ${contentRoots.size}")
        if (contentRoots.isNotEmpty()) {
            println("Content root 0: ${contentRoots[0]}")
        }
        val sourceRoots = ModuleRootManager.getInstance(module).sourceRoots
        println("sourceRoots: ${sourceRoots.size}") // WHY IS THIS 0 ?

        if (sourceRoots.isNotEmpty()) {
            println("Source root 0: ${sourceRoots[0]}")
        }

        println("Trying very hard to save our settings to the project: " + wizStep.dummySettings)
        TrainxProjectSettingsManager.getInstance(module.project)?.loadState(wizStep.dummySettings)

    }

    // ModuleBuilder abstract method
    @Throws(ConfigurationException::class)
    override fun setupRootModel(rootModel: ModifiableRootModel) {
        super.setupRootModel(rootModel)

        for (orderEntry in rootModel.orderEntries) {
            println("Here is an orderEntry: $orderEntry")
        }

        val sourcePaths: List<Pair<String, String>> = sourcePaths

        val contentEntry = doAddContentEntry(rootModel)
        if (contentEntry != null) {
            println("setupRootModel has this contentEntry: ${contentEntry.javaClass}")
            println("setupRootModel has this sourcePaths list: $sourcePaths")

            if (sourcePaths.isNotEmpty()) {
                for (sourcePath in sourcePaths) {
                    println("sourcePath here: ${sourcePath.first}")
                    val first = sourcePath.first
                    File(first).mkdirs()
                    val sourceRoot = LocalFileSystem.getInstance()
                            .refreshAndFindFileByPath(FileUtil.toSystemIndependentName(first))
                    if (sourceRoot != null) {
                        when (first) {
                            MODELS_FOLDER_NAME -> contentEntry.addSourceFolder(sourceRoot, TrainxSourceRootType.MODELS, TrainxSourceRootProperties("", true))
                            RESOURCES_FOLDER_NAME -> // not certain about this ...
                                contentEntry.addSourceFolder(sourceRoot, TrainxSourceRootType.TEXT_SOURCE, TrainxSourceRootProperties("", false))
                            TRAININGFILES_FOLDER_NAME -> contentEntry.addSourceFolder(sourceRoot, false, sourcePath.second)
                            else -> // not certain about this ...
                                contentEntry.addSourceFolder(sourceRoot, false, sourcePath.second)
                        }
                    }
                }
            }
        }

        // Should we also set the compiler output path in this method ?
        // postponed.
        // see JavaModuleBuilder as an example

    }

    private val wizStep = object : ModuleWizardStep() {

        val settingsPanel = TrainxProjectSettingsPanel(false, null) // there is no project yet
        val dummySettings = TrainxProjectSettings()

        override fun getComponent(): JComponent {
            settingsPanel.settingsToGUI(dummySettings)
            return settingsPanel
        }

        override fun updateDataModel() {
            settingsPanel.saveGuiDataToSettings(dummySettings)
            //println("wizStep.updateDataModel has these settings: $dummySettings")
        }

        override fun validate() = settingsPanel.validateFields()

    }

    companion object {
        internal const val TRAINX_PLUGIN_VERSION = 1
        @NonNls
        const val TRAININGFILES_FOLDER_NAME = "trainingfiles"
        @NonNls
        const val RESOURCES_FOLDER_NAME = "resources"
        @NonNls
        const val MODELS_FOLDER_NAME = "gen/models"
    }


}
