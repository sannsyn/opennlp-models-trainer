package com.sannsyn.onlp.module

import com.intellij.openapi.module.ModuleType
import com.sannsyn.onlp.filetypes.trainx.TrainxIcons

import javax.swing.Icon

class TrainxModuleType internal constructor() : ModuleType<TrainxModuleBuilder>("TRAINX_MODULE") {

  override fun createModuleBuilder(): TrainxModuleBuilder {
    return TrainxModuleBuilder()
  }

  override fun getName(): String {
    return "OpenNLP Models Trainer"
  }

  override fun getDescription(): String {
    return "Add simple OpenNLP models training and application to your IDE"
  }

  override fun getNodeIcon(isOpened: Boolean): Icon {
    return TrainxIcons.TRAINX
  }
}
