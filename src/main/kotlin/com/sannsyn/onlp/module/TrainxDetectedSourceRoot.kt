package com.sannsyn.onlp.module

import com.intellij.ide.util.projectWizard.importSources.DetectedSourceRoot

import java.io.File

class TrainxDetectedSourceRoot(root: File) : DetectedSourceRoot(root, "") {

    override fun getRootTypeName(): String {
        return "Trainx"
    }

}
