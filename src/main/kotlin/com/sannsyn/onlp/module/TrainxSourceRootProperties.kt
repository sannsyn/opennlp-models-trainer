package com.sannsyn.onlp.module

import org.jetbrains.jps.model.JpsSimpleElement
import org.jetbrains.jps.model.ex.JpsElementBase

/**
 * Mimicking java source root behaviour
 */
class TrainxSourceRootProperties(packagePrefix: String = "", forGeneratedSources: Boolean = false) :
        JpsElementBase<TrainxSourceRootProperties>(), JpsSimpleElement<TrainxSourceRootProperties> {
    private var myPackagePrefix = packagePrefix
    private var myForGeneratedSources: Boolean = forGeneratedSources

    private var packagePrefix: String
        get() = myPackagePrefix
        set(packagePrefix) {
            if (myPackagePrefix != packagePrefix) {
                myPackagePrefix = packagePrefix
            }
        }

    private var isForGeneratedSources: Boolean
        get() = myForGeneratedSources
        set(forGeneratedSources) {
            if (myForGeneratedSources != forGeneratedSources) {
                myForGeneratedSources = forGeneratedSources
            }
        }

    override fun setData(data: TrainxSourceRootProperties) {
        packagePrefix = data.myPackagePrefix
        isForGeneratedSources = data.myForGeneratedSources
    }

    override fun getData(): TrainxSourceRootProperties {
        return this
    }
}
